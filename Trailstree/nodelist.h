//header file for the node list
//Xiaopeng Oct 2004
//Ahmed Mahmood 2013

#ifndef XXX_NODELIST_H
#define XXX_NODELIST_H

#include "parameters.h"

#define MAXHASH_NL 1000

struct NodeListEntry
{
	int nid;
	void* nptr;
	NodeListEntry* nextHash; //pointer to the next hash node
	NodeListEntry* prevHash; //pointer to the prev hash node
	NodeListEntry* nextLeaf;  //pointer to the next leaf node in R-tree
	NodeListEntry* prevLeaf; //pointer to the prev leaf node in R-tree
};

class NodeList
{
public:			
	int size;	

public:				

	NodeListEntry* hashHead[MAXHASH_NL];
	NodeListEntry* hashTail[MAXHASH_NL];
	NodeListEntry* leafHead;
	NodeListEntry* leafTail;
	NodeListEntry* tokenPos;

	NodeListEntry *InsertHash(int id, void* ptr)
	{
		size++;

		//Build a new entry first
		NodeListEntry *NewEntry = new NodeListEntry;

		NewEntry->nid = id;
		NewEntry->nptr = ptr;

		//insert to the leaf tail
		if(leafTail == NULL)
		{
			NewEntry->nextLeaf = NULL;
			NewEntry->prevLeaf = NULL;
			leafHead = NewEntry;
			leafTail = NewEntry;
			tokenPos = leafHead;
		}
		else
		{
			NewEntry->nextLeaf = NULL;
			NewEntry->prevLeaf = leafTail;
			leafTail->nextLeaf = NewEntry;
			leafTail = NewEntry;
		}

		//insert to the hash table
		int slot = id%MAXHASH_NL;
		//Insert in the hash table structure	
		if(hashHead[slot] == NULL)
		{
			NewEntry->nextHash = NULL;
			NewEntry->prevHash = NULL;
			hashHead[slot] = NewEntry;
			hashTail[slot] = NewEntry;
			return NewEntry;
		}

		NodeListEntry* pos = hashHead[slot];
		while(pos && (pos->nid > NewEntry->nid))
		{
			pos = pos->nextHash;
		}

		if(pos == hashHead[slot])  //insert before head
		{
			hashHead[slot]->prevHash = NewEntry;
			NewEntry->nextHash = hashHead[slot];
			NewEntry->prevHash = NULL;
			hashHead[slot] = NewEntry;
		}
		else if(pos == NULL) //insert after tail
		{
			hashTail[slot]->nextHash = NewEntry;
			NewEntry->prevHash = hashTail[slot];
			NewEntry->nextHash = NULL;
			hashTail[slot] = NewEntry;
		}
		else  //insert in middle
		{
			NewEntry->nextHash = pos;
			NewEntry->prevHash = pos->prevHash;
			pos->prevHash->nextHash = NewEntry;
			pos->prevHash = NewEntry;
		}

		return NewEntry;
	}

	NodeListEntry *SearchNID(int id)
	{
		int slot = id%MAXHASH_NL;
		NodeListEntry *temp = hashHead[slot];
		while (temp!=NULL)
		{
			if (temp->nid==id)
				return temp;
			if(temp->nid < id)
				return NULL;
			temp = temp->nextHash;
		}
		return NULL;
	}

	NodeListEntry* GetNextTokenPos()
	{
		if(tokenPos)
		{
			NodeListEntry* tp = tokenPos;
			tokenPos = tokenPos->nextLeaf;
			if(!tokenPos)
				tokenPos = leafHead;
			return tp;
		}
		return NULL;
	}

	void DeleteHash(NodeListEntry *DelRec)
	{
		size--;

		int slot = DelRec->nid%MAXHASH_NL;

		//dequeue from leaf link
		if (DelRec->nextLeaf != NULL)
			DelRec->nextLeaf->prevLeaf = DelRec->prevLeaf;				
		if (DelRec->prevLeaf != NULL)
			DelRec->prevLeaf->nextLeaf = DelRec->nextLeaf;

		if(leafHead == DelRec)
		{
			if(DelRec->nextLeaf != NULL)
				leafHead = DelRec->nextLeaf;
			else
				leafHead = NULL;
		}

		if(leafTail==DelRec)
		{
			if(DelRec->prevLeaf != NULL)
				leafTail = DelRec->prevLeaf;
			else
				leafTail = NULL;
		}

		if(tokenPos==DelRec)
		{
			if(DelRec->nextLeaf != NULL)
				tokenPos = DelRec->nextLeaf;
			else
				tokenPos = leafHead;
		}

		//dequeue from hash link
		if (DelRec->nextHash != NULL)
			DelRec->nextHash->prevHash = DelRec->prevHash;				
		if (DelRec->prevHash != NULL)
			DelRec->prevHash->nextHash = DelRec->nextHash;

		if(hashHead[slot]==DelRec)
		{
			if(DelRec->nextHash != NULL)
				hashHead[slot] = DelRec->nextHash;
			else
				hashHead[slot] = NULL;
		}

		if(hashTail[slot]==DelRec)
		{
			if(DelRec->prevHash != NULL)
				hashTail[slot] = DelRec->prevHash;
			else
				hashTail[slot] = NULL;
		}

		delete DelRec;
	}

	void MoveToLeafTail(NodeListEntry *DelRec)
	{
		//dequeue from the leaf link
		if (DelRec->nextLeaf != NULL)
			DelRec->nextLeaf->prevLeaf = DelRec->prevLeaf;				
		if (DelRec->prevLeaf != NULL)
			DelRec->prevLeaf->nextLeaf = DelRec->nextLeaf;

		if(leafHead == DelRec)
		{
			if(DelRec->nextLeaf != NULL)
				leafHead = DelRec->nextLeaf;
			else
				leafHead = NULL;
		}

		if(leafTail==DelRec)
		{
			if(DelRec->prevLeaf != NULL)
				leafTail = DelRec->prevLeaf;
			else
				leafTail = NULL;
		}

		if(tokenPos==DelRec)
		{
			if(DelRec->nextLeaf != NULL)
				tokenPos = DelRec->nextLeaf;
			else
				tokenPos = leafHead;
		}

		//insert to the leaf tail
		if(leafTail == NULL)
		{
			DelRec->nextLeaf = NULL;
			DelRec->prevLeaf = NULL;
			leafHead = DelRec;
			leafTail = DelRec;
			tokenPos = DelRec;
		}
		else
		{
			DelRec->nextLeaf = NULL;
			DelRec->prevLeaf = leafTail;
			leafTail->nextLeaf = DelRec;
			leafTail = DelRec;
		}
	}


	void deleteAllEntries()
	{
		NodeListEntry *Temp1, *Temp2;

		for(int i=0; i<MAXHASH_NL; i++)
		{
			Temp1 = hashHead[i];
			while (Temp1!=NULL)
			{ 
				Temp2 = Temp1;
				Temp1 = Temp1->nextHash;
				delete Temp2;
			}
			
			hashHead[i] = NULL;
			hashTail[i] = NULL;
		}

		leafHead = NULL;
		leafTail = NULL;		
		tokenPos = NULL;
		size = 0;
	}

	NodeList()
	{
		size = 0;
		for(int i=0; i<MAXHASH_NL; i++)
		{
			hashHead[i] = NULL;
			hashTail[i] = NULL;
		}
		leafHead = NULL;
		leafTail = NULL;
		tokenPos = NULL;
	}

	~NodeList()
	{
		deleteAllEntries();
	}
};

#endif
