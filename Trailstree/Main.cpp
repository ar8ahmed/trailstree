#include <stdio.h>
#include <memory.h>
#include <crtdbg.h>
#include <time.h>
#include <windows.h>
#include <math.h>
#include <stdlib.h>

#include <fstream>
#include <iostream>
#include <math.h>
#include "RTree.h"
#include "MDM.h"
#include "SlidingWindowRum.h"
using namespace std;


// Use CRT Debug facility to dump memory leaks on app exit
#ifdef WIN32
// The following macros set and clear, respectively, given bits
// of the C runtime library debug flag, as specified by a bitmask.
#ifdef   _DEBUG
#define  SET_CRT_DEBUG_FIELD(a) \
	_CrtSetDbgFlag((a) | _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG))
#define  CLEAR_CRT_DEBUG_FIELD(a) \
	_CrtSetDbgFlag(~(a) & _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG))
#else
#define  SET_CRT_DEBUG_FIELD(a)   ((void) 0)
#define  CLEAR_CRT_DEBUG_FIELD(a) ((void) 0)
#endif
#endif //WIN32


static double Randdouble(double a_min, double a_max)
{
  const double ooMax = 1.0f / (double)(RAND_MAX+1);
  double retValue = ( (double)rand() * ooMax * (a_max - a_min) + a_min);
  assert(retValue >= a_min && retValue < a_max); // Paranoid check
  return retValue;
}
int compareType (const void * a, const void * b)
{
  if ( (*(LocEntry3D*)a).order <  (*(LocEntry3D*)b).order ) return -1;
  if ( (*(LocEntry3D*)a).order == (*(LocEntry3D*)b).order ) return 0;
  if (( *(LocEntry3D*)a).order >  (*(LocEntry3D*)b).order ) return 1;
}
void insertFromFile(char * fileName,SlidingWindowRum * SWtree){
	double maxTime =0;


	
	double duration =0;
	ifstream myfile ;
	myfile.open(fileName);
	//read file 
	int k=0;
	
	int id;
	double ts,x,y;
	
	while(k<MAX_NUMBER_UPDATES){	
		myfile>>id;
		myfile>>ts;
		myfile>>x;
		myfile>>y;
	
		if(id>=NUM_OBJECTS) continue;
		
		maxTime = ts;
		duration = duration + SWtree->update(id,x,y,ts,NOW_TIME);
		if(k%100000==0)
			cout<<"K="<<k<<"\n";
		k++;
	}
	
	SWtree->ShowStats();
	
	double updatetime = duration/CLOCKS_PER_SEC;
	printf("Maxtime = %.2f \n", maxTime);
	printf("Total update time = %.2f seconds\n", updatetime);
	printf("Number of UPDATES = %d\n", MAX_NUMBER_UPDATES);
	SWtree->tree.CountReset();
	printf("-------------------------------------------------------------\n");
	
}
void queryFromFile(char * fileName,SlidingWindowRum * SWtree){
	clock_t start, finish;
	double duration =0;
	ifstream myfile ;
	myfile.open(fileName);
	double tl,th,xmin,ymin,xmax,ymax;
	
	for(int i =0;i<NUM_QUERIES;i++){
		
		//cout<< i<<" query results  ";
		myfile>>tl;myfile>>th;myfile>>xmin;myfile>>ymin;myfile>>xmax;myfile>>ymax;
		
		duration += SWtree->query_spatio_temporal_range(xmin,ymin,xmax,ymax,tl,th);	
	}
	myfile.close();
	SWtree->ShowStats();
	double queryTime = duration/CLOCKS_PER_SEC;
	printf("Total query time = %.2f seconds\n", queryTime);
	cout<<"total retrieved results"<<SWtree->retrievedResults<<endl;
	SWtree->CountReset();
	duration = 0;
}
void queryFromFileExtended(char * fileName,SlidingWindowRum * SWtree,double queryXE,double queryYE,double queryTimeE){
	clock_t start, finish;
	double duration =0;
	ifstream myfile ;
	myfile.open(fileName);
	double tl,th,xmin,ymin,xmax,ymax;
	
	for(int i =0;i<NUM_QUERIES;i++){
		
		//cout<< i<<" query results  ";
		myfile>>tl;myfile>>th;myfile>>xmin;myfile>>ymin;myfile>>xmax;myfile>>ymax;
		
		duration += SWtree->query_spatio_temporal_range_extended(xmin,ymin,xmax,ymax,tl,th,queryXE,queryYE,queryTimeE);	
	}
	myfile.close();
	SWtree->ShowStats();
	double queryTime = duration/CLOCKS_PER_SEC;
	printf("Total query time = %.2f seconds\n", queryTime);
	cout<<"total retrieved results"<<SWtree->retrievedResults<<endl;
	SWtree->CountReset();
	duration = 0;
}
void queryFromFileEntireWindowTemporalRange(char * fileName,SlidingWindowRum * SWtree){
	clock_t start, finish;
	double duration =0;
	ifstream myfile ;
	myfile.open(fileName);
	double tl,th,xmin,ymin,xmax,ymax;
	
	for(int i =0;i<NUM_QUERIES;i++){
		
		//cout<< i<<" query results  ";
		myfile>>tl;myfile>>th;myfile>>xmin;myfile>>ymin;myfile>>xmax;myfile>>ymax;
		
		duration += SWtree->query_spatio_temporal_range(xmin,ymin,xmax,ymax,SWtree->tree.currentTimeStamp-DEFAULT_DURATION,SWtree->tree.currentTimeStamp);	
	}
	myfile.close();
	SWtree->ShowStats();
	double queryTime = duration/CLOCKS_PER_SEC;
	printf("Total query time = %.2f seconds\n", queryTime);
	cout<<"total retrieved results"<<SWtree->retrievedResults<<endl;
	SWtree->CountReset();
	duration = 0;
}
void multipleQueryFromFile(char * fileName,SlidingWindowRum * SWtree){
	double spatialSapnArr []={.01,.06,.1,.3};
	double timeSpanArr []={0.0,.1,.3,.4,.8};
	double defaultTimeSpan = .3;
	double defaultSpatialSpan = .06;
	for(int k = 0;k<4;k++){
		char newFileName [1000];
		cout<<"----------------------------------------------------------------------------------------"<<endl;
		cout<<"query spatial = "<<spatialSapnArr[k]<<" temporal = "<<defaultTimeSpan<<endl;
		sprintf( newFileName, "%s_%.2lf_%.2lf.txt",fileName ,spatialSapnArr[k], defaultTimeSpan );
		queryFromFile(newFileName,SWtree);
	}
	for(int k = 0;k<5;k++){
		char newFileName [1000];
		cout<<"----------------------------------------------------------------------------------------"<<endl;
		cout<<"query spatial = "<<defaultSpatialSpan<<" temporal = "<<timeSpanArr [k]<<endl;
		sprintf( newFileName, "%s_%.2lf_%.2lf.txt",fileName ,defaultSpatialSpan, timeSpanArr [k] );
		queryFromFile(newFileName,SWtree);
	}
	char newFileName [1000];
	cout<<"----------------------------------------------------------------------------------------"<<endl;
	cout<<"query spatial = "<<defaultSpatialSpan<<" temporal = "<<1<<endl;
	sprintf( newFileName, "%s_%.2lf_%.2lf.txt",fileName ,defaultSpatialSpan, .8 );
	queryFromFileEntireWindowTemporalRange(newFileName, SWtree);
	printf("-------------------------------------------------------------\n");
}
void test6(){
	printf("-------------------------------------------------------------\n");
	printf("window size = %d\n",DEFAULT_DURATION);
	SlidingWindowRum *  SWtree = new SlidingWindowRum();
	insertFromFile("E:\\work\\purdue\\database\\research\\datasets\\trajecotry\\GSTD\\FinalGSTDout.txt", SWtree);
	//insertFromFile("D:\\test dataset\\tdrive dataset\\out.txt", SWtree);
	//insertFromFile("E:\\brinkhoffvariabledurationdataset\\brinkhoffdataset.txt", SWtree);
	//insertFromFile("E:\\uniformdataset\\one_million\\UniformDataSet_one_million.txt",SWtree);
	//insertFromFile("E:\\uniformdataset\\five_million\\UniformDataSet_five_million.txt",SWtree);
	//insertFromFile("E:\\uniformdataset\\two_half_million\\UniformDataSet_two_half_million.txt", SWtree);
	//insertFromFile("E:\\work\\purdue\\database\\research\\datasets\\Geolife Trajectories 1.3\\geolife", SWtree);
	//printf("Testing spatial extent effect\n");
	//cout<< "spatial range = .1 temporal range = 0.3"<<endl;
	//queryFromFile("E:\\work\\purdue\\database\\research\\datasets\\Geolife Trajectories 1.3\\geolife_one_million_spat_0.1_temp_0.3.txt", SWtree);
	//multipleQueryFromFile("E:\\work\\purdue\\database\\research\\datasets\\Geolife Trajectories 1.3\\multiple window query\\window_5000000",SWtree);
	//multipleQueryFromFile("D:\\test dataset\\tdrive dataset\\multiplewindow\\window_50000",SWtree);
	//multipleQueryFromFile("E:\\work\\purdue\\database\\research\\datasets\\trajecotry\\GSTD\\gstdqueries_window_200",SWtree);
	/*
	printf("-------------------------------------------------------------\n");
	printf("Testing spatial extent effect\n");
	cout<< "spatial range = .01 temporal range = 0.3"<<endl;
	queryFromFile("E:\\work\\purdue\\database\\research\\datasets\\Geolife Trajectories 1.3\\geolife_two_half_million_spat_0.01_temp_0.3_.txt", SWtree);
	printf("-------------------------------------------------------------\n");
	cout<< "spatial range = .06 temporal range = 0.3"<<endl;
	queryFromFile("E:\\work\\purdue\\database\\research\\datasets\\Geolife Trajectories 1.3\\geolife_two_half_million_spat_0.06_temp_0.3_.txt", SWtree);
	printf("-------------------------------------------------------------\n");
	cout<< "spatial range = .1 temporal range = 0.3"<<endl;
	queryFromFile("E:\\work\\purdue\\database\\research\\datasets\\Geolife Trajectories 1.3\\geolife_two_half_million_spat_0.1_temp_0.3_.txt", SWtree);
	printf("-------------------------------------------------------------\n");
	cout<< "spatial range = .3 temporal range = 0.3"<<endl;
	queryFromFile("E:\\work\\purdue\\database\\research\\datasets\\Geolife Trajectories 1.3\\geolife_two_half_million_spat_0.3_temp_0.3_.txt", SWtree);
	printf("-------------------------------------------------------------\n");
	printf("-------------------------------------------------------------\n");
	printf("Testing temporal extent effect\n");
	cout<< "spatial range = .06 temporal range = 0.0"<<endl;
	queryFromFile("E:\\work\\purdue\\database\\research\\datasets\\Geolife Trajectories 1.3\\geolife_two_half_million_spat_0.06_temp_0.0_.txt", SWtree);
	printf("-------------------------------------------------------------\n");	
	cout<< "spatial range = .06 temporal range = 0.1"<<endl;
	queryFromFile("E:\\work\\purdue\\database\\research\\datasets\\Geolife Trajectories 1.3\\geolife_two_half_million_spat_0.06_temp_0.1_.txt", SWtree);
	printf("-------------------------------------------------------------\n");
	cout<< "spatial range = .06 temporal range = .4"<<endl;
	queryFromFile("E:\\work\\purdue\\database\\research\\datasets\\Geolife Trajectories 1.3\\geolife_two_half_million_spat_0.06_temp_0.4_.txt", SWtree);
	printf("-------------------------------------------------------------\n");
	cout<< "spatial range = .06 temporal range = .8"<<endl;
	queryFromFile("E:\\work\\purdue\\database\\research\\datasets\\Geolife Trajectories 1.3\\geolife_two_half_million_spat_0.06_temp_0.8_.txt", SWtree);
	printf("-------------------------------------------------------------\n");
	cout<< "spatial range = .06 temporal range = 1"<<endl;
	queryFromFileEntireWindowTemporalRange("E:\\work\\purdue\\database\\research\\datasets\\Geolife Trajectories 1.3\\geolife_two_half_million_spat_0.06_temp_0.8_.txt", SWtree);
	printf("-------------------------------------------------------------\n");
	*/
	/*
	printf("-------------------------------------------------------------\n");
	printf("Testing spatial extent effect\n");
	cout<< "spatial range = .01 temporal range = 0.3"<<endl;
	queryFromFile("D:\\test dataset\\tdrive dataset\\tdrive_two_half_million_spat_0.01_temp_0.3.txt", SWtree);
	printf("-------------------------------------------------------------\n");
	cout<< "spatial range = .06 temporal range = 0.3"<<endl;
	queryFromFile("D:\\test dataset\\tdrive dataset\\tdrive_two_half_million_spat_0.06_temp_0.3.txt", SWtree);
	printf("-------------------------------------------------------------\n");
	cout<< "spatial range = .1 temporal range = 0.3"<<endl;
	queryFromFile("D:\\test dataset\\tdrive dataset\\tdrive_two_half_million_spat_0.1_temp_0.3.txt", SWtree);
	printf("-------------------------------------------------------------\n");
	cout<< "spatial range = .3 temporal range = 0.3"<<endl;
	queryFromFile("D:\\test dataset\\tdrive dataset\\tdrive_two_half_million_spat_0.3_temp_0.3.txt", SWtree);
	printf("-------------------------------------------------------------\n");	
	printf("-------------------------------------------------------------\n");
	printf("Testing temporal extent effect\n");
	printf("-------------------------------------------------------------\n");
	cout<< "spatial range = .06 temporal range = 0.0"<<endl;
	queryFromFile("D:\\test dataset\\tdrive dataset\\tdrive_two_half_million_spat_0.06_temp_0.0.txt", SWtree);
	printf("-------------------------------------------------------------\n");
	cout<< "spatial range = .06 temporal range = 0.1"<<endl;
	queryFromFile("D:\\test dataset\\tdrive dataset\\tdrive_two_half_million_spat_0.06_temp_0.1.txt", SWtree);
	printf("-------------------------------------------------------------\n");
	cout<< "spatial range = .06 temporal range = .4"<<endl;
	queryFromFile("D:\\test dataset\\tdrive dataset\\tdrive_two_half_million_spat_0.06_temp_0.4.txt", SWtree);
	printf("-------------------------------------------------------------\n");
	cout<< "spatial range = .06 temporal range = .8"<<endl;
	queryFromFile("D:\\test dataset\\tdrive dataset\\tdrive_two_half_million_spat_0.06_temp_0.8.txt", SWtree);
	printf("-------------------------------------------------------------\n");
	cout<< "spatial range = .06 temporal range = 1"<<endl;
	queryFromFileEntireWindowTemporalRange("D:\\test dataset\\tdrive dataset\\tdrive_two_half_million_spat_0.06_temp_0.8.txt", SWtree);
	printf("-------------------------------------------------------------\n");
	
	*/
	
	
	//queryFromFile("D:\\test dataset\\tdrive dataset\\tdrive_one_million_spat_0.1_temp_0.3.txt", SWtree);
	//cout<< "spatial range = .1 temporal range = 0.1"<<endl;
	//queryFromFile("E:\\work\\purdue\\database\\research\\datasets\\Geolife Trajectories 1.3\\geolife_one_million__time_mapped_spat_0.1_temp_0.1.txt", SWtree);
	//queryFromFile("E:\\work\\purdue\\database\\research\\datasets\\Geolife Trajectories 1.3\\geolife_one_million_spat_0.3_temp_0.3.txt", SWtree);

	/*
	printf("-------------------------------------------------------------\n");
	printf("Testing spatial extent effect\n");
	cout<< "spatial range = .01 temporal range = 0.3"<<endl;
	queryFromFile("E:\\uniformdataset\\two_half_million\\UniformDataSet_two_half_million_spat_0.01_temp_0.3.txt", SWtree);
	
	printf("-------------------------------------------------------------\n");
	cout<< "spatial range = .06 temporal range = 0.3"<<endl;
	queryFromFile("E:\\uniformdataset\\two_half_million\\UniformDataSet_two_half_million_spat_0.06_temp_0.3.txt", SWtree);
	
	printf("-------------------------------------------------------------\n");
	cout<< "spatial range = .1 temporal range = 0.3"<<endl;
	queryFromFile("E:\\uniformdataset\\two_half_million\\UniformDataSet_two_half_million_spat_0.1_temp_0.3.txt", SWtree);
	
	printf("-------------------------------------------------------------\n");
	cout<< "spatial range = .3 temporal range = 0.3"<<endl;
	queryFromFile("E:\\uniformdataset\\two_half_million\\UniformDataSet_two_half_million_spat_0.3_temp_0.3.txt", SWtree);

	printf("-------------------------------------------------------------\n");
	
	
	printf("-------------------------------------------------------------\n");
	printf("Testing temporal extent effect\n");
	cout<< "spatial range = .06 temporal range = 0.0"<<endl;
	queryFromFile("E:\\uniformdataset\\two_half_million\\UniformDataSet_two_half_million_spat_0.06_temp_0.0.txt", SWtree);

	printf("-------------------------------------------------------------\n");
	
	cout<< "spatial range = .06 temporal range = 0.1"<<endl;
	queryFromFile("E:\\uniformdataset\\two_half_million\\UniformDataSet_two_half_million_spat_0.06_temp_0.1.txt", SWtree);

	printf("-------------------------------------------------------------\n");
	cout<< "spatial range = .06 temporal range = .4"<<endl;
	queryFromFile("E:\\uniformdataset\\two_half_million\\UniformDataSet_two_half_million_spat_0.06_temp_0.4.txt", SWtree);

	printf("-------------------------------------------------------------\n");
	cout<< "spatial range = .06 temporal range = .8"<<endl;
	queryFromFile("E:\\uniformdataset\\two_half_million\\UniformDataSet_two_half_million_spat_0.06_temp_0.8.txt", SWtree);
	
	printf("-------------------------------------------------------------\n");
	
	cout<< "spatial range = .06 temporal range = 1"<<endl;
	queryFromFileEntireWindowTemporalRange("E:\\uniformdataset\\two_half_million\\UniformDataSet_two_half_million_spat_0.06_temp_0.8.txt", SWtree);
	printf("-------------------------------------------------------------\n");
	*/
	
	/*
	printf("-------------------------------------------------------------\n");
	cout<< "spatial range = .01 temporal range = .3"<<endl;
	queryFromFile("E:\\brinkhoffvariabledurationdataset\\query_spat_0.01_temp_0.3.txt", SWtree);
	
	printf("-------------------------------------------------------------\n");
	cout<< "spatial range = .06 temporal range = .3"<<endl;
	queryFromFile("E:\\brinkhoffvariabledurationdataset\\query_spat_0.06_temp_0.3.txt", SWtree);
	printf("-------------------------------------------------------------\n");
	cout<< "spatial range = .1 temporal range = .3"<<endl;
	queryFromFile("E:\\brinkhoffvariabledurationdataset\\query_spat_0.1_temp_0.3.txt", SWtree);
	printf("-------------------------------------------------------------\n");
	cout<< "spatial range = .3 temporal range = .3"<<endl;
	queryFromFile("E:\\brinkhoffvariabledurationdataset\\query_spat_0.3_temp_0.3.txt", SWtree);
	printf("-------------------------------------------------------------\n");
	
	
	printf("-------------------------------------------------------------\n");
	printf("Testing temporal extent effect\n");
	cout<< "spatial range = .06 temporal range = .0"<<endl;
	queryFromFile("E:\\brinkhoffvariabledurationdataset\\query_spat_0.06_temp_0.0.txt", SWtree);
	printf("-------------------------------------------------------------\n");
	cout<< "spatial range = .06 temporal range = .1"<<endl;
	queryFromFile("E:\\brinkhoffvariabledurationdataset\\query_spat_0.06_temp_0.1.txt", SWtree);
	printf("-------------------------------------------------------------\n");
	cout<< "spatial range = .06 temporal range = .4"<<endl;
	queryFromFile("E:\\brinkhoffvariabledurationdataset\\query_spat_0.06_temp_0.4.txt", SWtree);
	printf("-------------------------------------------------------------\n");
	cout<< "spatial range = .06 temporal range = .8"<<endl;
	queryFromFile("E:\\brinkhoffvariabledurationdataset\\query_spat_0.06_temp_0.8.txt", SWtree);
	printf("-------------------------------------------------------------\n");
	
	cout<< "spatial range = .06 temporal range = 1"<<endl;
	queryFromFileEntireWindowTemporalRange("E:\\brinkhoffvariabledurationdataset\\query_spat_0.06_temp_0.8.txt", SWtree);
	printf("-------------------------------------------------------------\n");
	*/

	delete   SWtree ;
}


int main(int argc, char* argv[])
{
	test6();
	printf("Test done\n");
	int x;
	cin>>x;
	return 0;
}