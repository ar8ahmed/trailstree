//Ahmed Mahmood 2013

#ifndef RTREEW_H
#define RTREEW_H

#include <stdio.h>
#include <memory.h>
#include <crtdbg.h>
#include <time.h>
#include <windows.h>
#include <math.h>
#include <stdlib.h>

#include <assert.h>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <vector>
#include <unordered_map>
#include <math.h>
#include "RTree.h"

using namespace std;
typedef RTree<LocEntry3D*, double,3> LocEntry3DTree;

struct resultNode{
	int oid;
	double x,y,ts,te;
};
struct Point
{
    double x;
    double y;
};
struct queryContex{
	 double qtl,qth,qt,qxmin,qxmax,qymin,qymax,qoid,queryType,totalQueryResult,totalCorrectQueryResult;
	 vector<resultNode *> * resultList;
};

struct queryContexExtended{
	 double qtl,qth,qt,qxmin,qxmax,qymin,qymax,qxe,qye,qte,qoid,queryType,totalQueryResult,totalCorrectQueryResult;
	
	// vector<resultNode *> * resultList;
	 vector<resultNode *> * regionL1;
	 vector<resultNode *> * regionL2;
	 vector<resultNode *> * regionL3;
	 vector<resultNode *> * regionL4;
	 vector<resultNode *> * regionC1;
	 vector<resultNode *> * regionC2;
	 vector<resultNode *> * regionC3;
	 vector<resultNode *> * regionC4;
	 vector<resultNode *> * regionU1;
	 vector<resultNode *> * regionU2;
	 vector<resultNode *> * regionU3;
	 vector<resultNode *> * regionU4;
};
class SlidingWindowRum{
	
private:
	
int maxOIDSoFar;

public:	
 LocEntry3DTree tree;
int totalTreeEntries;
int retrievedResults;

//to randomize a double number in a range
static double Randdouble(double a_min, double a_max)
{
  const double ooMax = 1.0f / (double)(RAND_MAX+1);
  double retValue = ( (double)rand() * ooMax * (a_max - a_min) + a_min);
  ASSERT(retValue >= a_min && retValue < a_max); // Paranoid check
  return retValue;
}
SlidingWindowRum(){
	 maxOIDSoFar =0;
	 totalTreeEntries =0;
	 retrievedResults =0;
}
~SlidingWindowRum(){

}
static bool compareResultListNodes(resultNode * a, resultNode* b){
	return (a->oid < b->oid || (a->oid == b->oid && a->ts < b->ts));
}

void freeResultList(queryContex * con){   
	for(int i =0; i < con->resultList->size() ;i++){
		//	cout<<con->resultList->at(i)->oid<<"  "<<con->resultList->at(i)->ts<<"   "<<con->resultList->at(i)->x<<con->resultList->at(i)->y<<endl;
		delete( con->resultList->at(i) );
	}
}
// tkuznets
void freeResultListExtended(queryContexExtended * con){   
	int i =0;
	while (i < con->regionL1->size() ) {
		delete( con->regionL1->at(i) );
		i++;
	}
    i =0;
	while (i < con->regionL2->size() ) {
		delete( con->regionL2->at(i) );
		i++;
	}
	i =0;
	while (i < con->regionL3->size() ) {
		delete( con->regionL3->at(i) );
		i++;
	}
	i =0;
	while (i < con->regionL4->size() ) {
		delete( con->regionL4->at(i) );
		i++;
	}
	i =0;
	while (i < con->regionC1->size() ) {
		delete( con->regionC1->at(i) );
		i++;
	}
	i =0;
	while (i < con->regionC2->size() ) {
		delete( con->regionC2->at(i) );
		i++;
	}
	i =0;
	while (i < con->regionC3->size() ) {
		delete( con->regionC3->at(i) );
		i++;
	}
	i =0;
	while (i < con->regionC4->size() ) {
		delete( con->regionC4->at(i) );
		i++;
	}
	i =0;
	while (i < con->regionU1->size() ) {
		delete( con->regionU1->at(i) );
		i++;
	}
	i =0;
	while (i < con->regionU2->size() ) {
		delete( con->regionU2->at(i) );
		i++;
	}
	i =0;
	while (i < con->regionU3->size() ) {
		delete( con->regionU3->at(i) );
		i++;
	}
	i =0;
	while (i < con->regionU4->size() ) {
		delete( con->regionU4->at(i) );
		i++;
	}
}

/// A callback function to obtain query results in this implementation
static bool _cdecl QueryResultCallback(LocEntry3D* a_data, void* con)
{
	
	//resultNode * p = new resultNode;
	//p->oid = a_data->oid;
	//p->x = a_data->vmin.v[0];
	//p->y = a_data->vmin.v[1];
	//p->ts = a_data->vmin.v[2];//a_data->start;
	//p->te = a_data->vmax.v[2];//a_data->end;


	//((queryContex *)con)->resultList->push_back(p);	
	return true;
}
static bool checkInBox(double xmin,double ymin,double xmax,double ymax,long tl,long th,LocEntry3D* a_data, vector<resultNode *> * list){

	if(a_data->vmin.v[0]>=xmin &&a_data->vmin.v[0]<=xmax &&
		a_data->vmin.v[1]>=ymin &&a_data->vmin.v[1]<=ymax &&
		a_data->vmin.v[2]<=th &&a_data->vmax.v[2]>=tl ){
			resultNode * p = new resultNode;
			p->oid = a_data->oid;
			p->x = a_data->vmin.v[0];
			p->y = a_data->vmin.v[1];
			p->ts = a_data->vmin.v[2];//a_data->start;
			p->te = a_data->vmax.v[2];//a_data->end;
			list->push_back(p);
			//printf("\n range: %f %f %f %f %ld %ld \n", xmin, ymin, xmax, ymax, tl, th);
			//printf("\n push back: %f %f %f %f \n", p->x, p->y, p->ts, p->te);
			return true; // tkuzn
	}
	else {
		//printf("\n --range: %f %f %f %f %ld %ld \n", xmin, ymin, xmax, ymax, tl, th);
		//printf("\n --push back: %f %f %f %f \n", a_data->vmin.v[0], a_data->vmin.v[1], a_data->vmin.v[2], a_data->vmax.v[2]);
	}
	return true; // tkuzn
}

/// A callback function to obtain query results in this implementation
static bool _cdecl QueryResultCallbackExtended(LocEntry3D* a_data, void* con)
{
	if(a_data->vmin.v[0]>=((queryContexExtended *)con)->qxmin &&a_data->vmin.v[0]<=((queryContexExtended *)con)->qxmax &&
		a_data->vmin.v[1]>=((queryContexExtended *)con)->qymin &&a_data->vmin.v[1]<=((queryContexExtended *)con)->qymax &&
		a_data->vmin.v[2]<=((queryContexExtended *)con)->qth &&a_data->vmax.v[2]>=((queryContexExtended *)con)->qtl )
		//this is inside original query range
		((queryContex *)con)->totalCorrectQueryResult++;
	//printf("L1\n");
	checkInBox(((queryContexExtended *)con)->qxmin,((queryContexExtended *)con)->qymin,((queryContexExtended *)con)->qxmin+((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qymax,((queryContexExtended *)con)->qtl-((queryContexExtended *)con)->qte,((queryContexExtended *)con)->qtl,a_data,((queryContexExtended *)con)->regionL1);
	checkInBox(((queryContexExtended *)con)->qxmin-((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qymin-((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qxmin,((queryContexExtended *)con)->qymax+((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qtl,((queryContexExtended *)con)->qtl+((queryContexExtended *)con)->qte,a_data,((queryContexExtended *)con)->regionL1);
	//printf("L2\n");
	checkInBox(((queryContexExtended *)con)->qxmax-((queryContexExtended *)con)->qxe ,((queryContexExtended *)con)->qymin,((queryContexExtended *)con)->qxmax,((queryContexExtended *)con)->qymax,((queryContexExtended *)con)->qtl-((queryContexExtended *)con)->qte,((queryContexExtended *)con)->qtl,a_data,((queryContexExtended *)con)->regionL2);
	checkInBox(((queryContexExtended *)con)->qxmax,((queryContexExtended *)con)->qymin-((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qxmax+((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qymax+((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qtl,((queryContexExtended *)con)->qtl+((queryContexExtended *)con)->qte,a_data,((queryContexExtended *)con)->regionL2);
	//printf("L3\n");
	checkInBox(((queryContexExtended *)con)->qxmin,((queryContexExtended *)con)->qymin,((queryContexExtended *)con)->qxmax,((queryContexExtended *)con)->qymin+((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qtl-((queryContexExtended *)con)->qte,((queryContexExtended *)con)->qtl,a_data,((queryContexExtended *)con)->regionL3);
	checkInBox(((queryContexExtended *)con)->qxmin-((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qymin-((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qxmax+((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qymin,((queryContexExtended *)con)->qtl,((queryContexExtended *)con)->qtl+((queryContexExtended *)con)->qte,a_data,((queryContexExtended *)con)->regionL3);
	//printf("L4\n");
	checkInBox(((queryContexExtended *)con)->qxmin,((queryContexExtended *)con)->qymax-((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qxmax,((queryContexExtended *)con)->qymax,((queryContexExtended *)con)->qtl-((queryContexExtended *)con)->qte,((queryContexExtended *)con)->qtl,a_data,((queryContexExtended *)con)->regionL4);
	checkInBox(((queryContexExtended *)con)->qxmin-((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qymax,((queryContexExtended *)con)->qxmax+((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qymax+((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qtl,((queryContexExtended *)con)->qtl+((queryContexExtended *)con)->qte,a_data,((queryContexExtended *)con)->regionL4);
	
	//printf("U1\n");
	checkInBox(((queryContexExtended *)con)->qxmin-((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qymin,((queryContexExtended *)con)->qxmin,((queryContexExtended *)con)->qymax,((queryContexExtended *)con)->qth-((queryContexExtended *)con)->qte,((queryContexExtended *)con)->qth,a_data,((queryContexExtended *)con)->regionU1);
	checkInBox(((queryContexExtended *)con)->qxmin,((queryContexExtended *)con)->qymin-((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qxmin+((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qymax+((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qth,((queryContexExtended *)con)->qth+((queryContexExtended *)con)->qte,a_data,((queryContexExtended *)con)->regionU1);
	//printf("U2\n");
	checkInBox(((queryContexExtended *)con)->qxmax ,((queryContexExtended *)con)->qymin,((queryContexExtended *)con)->qxmax+((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qymax,((queryContexExtended *)con)->qth-((queryContexExtended *)con)->qte,((queryContexExtended *)con)->qth,a_data,((queryContexExtended *)con)->regionU2);
	checkInBox(((queryContexExtended *)con)->qxmax-((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qymin-((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qxmax,((queryContexExtended *)con)->qymax+((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qth,((queryContexExtended *)con)->qth+((queryContexExtended *)con)->qte,a_data,((queryContexExtended *)con)->regionU2);
	//printf("U3\n");
	checkInBox(((queryContexExtended *)con)->qxmin,((queryContexExtended *)con)->qymin-((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qxmax,((queryContexExtended *)con)->qymin,((queryContexExtended *)con)->qth-((queryContexExtended *)con)->qte,((queryContexExtended *)con)->qth,a_data,((queryContexExtended *)con)->regionU3);
	checkInBox(((queryContexExtended *)con)->qxmin-((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qymin,((queryContexExtended *)con)->qxmax+((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qymin+((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qth,((queryContexExtended *)con)->qth+((queryContexExtended *)con)->qte,a_data,((queryContexExtended *)con)->regionU3);
	//printf("U4\n");
	checkInBox(((queryContexExtended *)con)->qxmin,((queryContexExtended *)con)->qymax,((queryContexExtended *)con)->qxmax,((queryContexExtended *)con)->qymax+((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qth-((queryContexExtended *)con)->qte,((queryContexExtended *)con)->qth,a_data,((queryContexExtended *)con)->regionU4);
	checkInBox(((queryContexExtended *)con)->qxmin-((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qymax-((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qxmax+((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qymax,((queryContexExtended *)con)->qth,((queryContexExtended *)con)->qth+((queryContexExtended *)con)->qte,a_data,((queryContexExtended *)con)->regionU4);
	
	//printf("C1\n");
	checkInBox(((queryContexExtended *)con)->qxmin,((queryContexExtended *)con)->qymin-((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qxmin+((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qymin,((queryContexExtended *)con)->qtl,((queryContexExtended *)con)->qth,a_data,((queryContexExtended *)con)->regionC1);
	checkInBox(((queryContexExtended *)con)->qxmin-((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qymin,((queryContexExtended *)con)->qxmin,((queryContexExtended *)con)->qymin+((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qtl,((queryContexExtended *)con)->qth,a_data,((queryContexExtended *)con)->regionC1);
	//printf("C2\n");
	checkInBox(((queryContexExtended *)con)->qxmin-((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qymax-((queryContexExtended *)con)->qye,	((queryContexExtended *)con)->qxmin,((queryContexExtended *)con)->qymax,((queryContexExtended *)con)->qtl,((queryContexExtended *)con)->qth,a_data,((queryContexExtended *)con)->regionC2);
	checkInBox(((queryContexExtended *)con)->qxmin,	((queryContexExtended *)con)->qymax,((queryContexExtended *)con)->qxmin+((queryContexExtended *)con)->qxe,	((queryContexExtended *)con)->qymax+((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qtl,((queryContexExtended *)con)->qth,a_data,((queryContexExtended *)con)->regionC2);
	//printf("C3\n");
	checkInBox(((queryContexExtended *)con)->qxmax,((queryContexExtended *)con)->qymax-((queryContexExtended *)con)->qye,	((queryContexExtended *)con)->qxmax+((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qymax,((queryContexExtended *)con)->qtl,((queryContexExtended *)con)->qth,a_data,((queryContexExtended *)con)->regionC3);
	checkInBox(((queryContexExtended *)con)->qxmax-((queryContexExtended *)con)->qxe,	((queryContexExtended *)con)->qymax,((queryContexExtended *)con)->qxmax,	((queryContexExtended *)con)->qymax+((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qtl,((queryContexExtended *)con)->qth,a_data,((queryContexExtended *)con)->regionC3);
	//printf("C4\n");
	checkInBox(((queryContexExtended *)con)->qxmax-((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qymin-((queryContexExtended *)con)->qye,	((queryContexExtended *)con)->qxmax,((queryContexExtended *)con)->qymin,((queryContexExtended *)con)->qtl,((queryContexExtended *)con)->qth,a_data,((queryContexExtended *)con)->regionC4);
	checkInBox(((queryContexExtended *)con)->qxmax,	((queryContexExtended *)con)->qymin,((queryContexExtended *)con)->qxmax+((queryContexExtended *)con)->qxe,	((queryContexExtended *)con)->qymin+((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qtl,((queryContexExtended *)con)->qth,a_data,((queryContexExtended *)con)->regionC4);
	
	return true;
}
//insertion of an object in the the Tree 
double insert(int oid,double x,double y , double ts,double te){
	clock_t start, finish;
	double  duration = 0;
	if(te>tree.currentTimeStamp)tree.currentTimeStamp = ts;
	LocEntry3D* newobj = new LocEntry3D;
	newobj->oid = oid;
	newobj->vmin.v[0]=x;
	newobj->vmin.v[1]=y;
	newobj->vmin.v[2]=ts;
	newobj->vmax.v[0]=x;
	newobj->vmax.v[1]=y;
	newobj->vmax.v[2]=te;
	newobj->order = tree.globalCnt++;
	//newobj->start = ts;
	//newobj->end = te;
	start = clock();
	tree.Register(newobj->vmin.v, newobj->vmax.v, newobj);
	finish= clock();
	totalTreeEntries++;
	duration = (double)(finish-start);
	return duration;
}
//update of a moving object in the 
double update(int oid,double x,double y , double ts,double te){
	clock_t start, finish;
	double  duration = 0;
	if(ts>te)
		cout<<"error"<<endl;
	if(te>tree.currentTimeStamp)tree.currentTimeStamp = ts;
	 LocEntry3D* newobj = new LocEntry3D;
	newobj->oid = oid;
	newobj->vmin.v[0]=x;
	newobj->vmin.v[1]=y;
	newobj->vmin.v[2]=ts;
	newobj->vmax.v[0]=x;
	newobj->vmax.v[1]=y;		
	newobj->vmax.v[2]=te;
	newobj->order = tree.globalCnt++;
	//newobj->start = ts;
	//newobj->end = te;
	start = clock();
	tree.Update(newobj->vmin.v, newobj->vmax.v, newobj);
	finish= clock();
	totalTreeEntries++;
	duration = (double)(finish-start);
	return duration;
}

double query_spatial_time_slice(double xmin, double ymin ,double xmax, double ymax, double tl, double th, void * con,bool recent){
	  clock_t start, finish;
      double  duration = 0;
	  Vector3D searchMin3D = Vector3D(xmin,ymin,tl);
	  Vector3D searchMax3D = Vector3D(xmax,ymax,th);
	  start = clock();
	  int found  = tree.Search(searchMin3D.v, searchMax3D.v, &QueryResultCallback, con);
	 
	  finish = clock();
	  retrievedResults +=found;
	  
	//  cout<<found<<endl;
	  
	  duration = (double)(finish-start);
	  freeResultList((queryContex *)con);
	  delete con;
	  return duration;
}

int refine_exteneded_query_result(double xmin, double ymin ,double xmax, double ymax, double tl, double th, void * con,double queryXE,double queryYE,double queryTE){

	if (!(*((queryContexExtended *)con)->regionL1).empty()) // tkuznets "empty" added
		std::sort((*((queryContexExtended *)con)->regionL1).begin(),(*((queryContexExtended *)con)->regionL1).end(),compareResultListNodes);
	if (!(*((queryContexExtended *)con)->regionL2).empty()) 
		std::sort((*((queryContexExtended *)con)->regionL2).begin(),(*((queryContexExtended *)con)->regionL2).end(),compareResultListNodes);
	if (!(*((queryContexExtended *)con)->regionL3).empty()) 
		std::sort((*((queryContexExtended *)con)->regionL3).begin(),(*((queryContexExtended *)con)->regionL3).end(),compareResultListNodes);
	if (!(*((queryContexExtended *)con)->regionL4).empty()) 
		std::sort((*((queryContexExtended *)con)->regionL4).begin(),(*((queryContexExtended *)con)->regionL4).end(),compareResultListNodes);
	
	if (!(*((queryContexExtended *)con)->regionU1).empty()) 
		std::sort((*((queryContexExtended *)con)->regionU1).begin(),(*((queryContexExtended *)con)->regionU1).end(),compareResultListNodes);
	if (!(*((queryContexExtended *)con)->regionU2).empty()) 
		std::sort((*((queryContexExtended *)con)->regionU2).begin(),(*((queryContexExtended *)con)->regionU2).end(),compareResultListNodes);
	if (!(*((queryContexExtended *)con)->regionU3).empty()) 
		std::sort((*((queryContexExtended *)con)->regionU3).begin(),(*((queryContexExtended *)con)->regionU3).end(),compareResultListNodes);
	if (!(*((queryContexExtended *)con)->regionU4).empty()) 
		std::sort((*((queryContexExtended *)con)->regionU4).begin(),(*((queryContexExtended *)con)->regionU4).end(),compareResultListNodes);

	if (!(*((queryContexExtended *)con)->regionC1).empty()) 
		std::sort((*((queryContexExtended *)con)->regionC1).begin(),(*((queryContexExtended *)con)->regionC1).end(),compareResultListNodes);
	if (!(*((queryContexExtended *)con)->regionC2).empty()) 
		std::sort((*((queryContexExtended *)con)->regionC2).begin(),(*((queryContexExtended *)con)->regionC2).end(),compareResultListNodes);
	if (!(*((queryContexExtended *)con)->regionC3).empty()) 
		std::sort((*((queryContexExtended *)con)->regionC3).begin(),(*((queryContexExtended *)con)->regionC3).end(),compareResultListNodes);
	if (!(*((queryContexExtended *)con)->regionC4).empty()) 
		std::sort((*((queryContexExtended *)con)->regionC4).begin(),(*((queryContexExtended *)con)->regionC4).end(),compareResultListNodes);

	int count =0;
	//x,t

	//printf("L1");
	
	count+=  findInterSections(((queryContexExtended *)con)->regionL1,
		((queryContexExtended *)con)->qxmin,((queryContexExtended *)con)->qtl,((queryContexExtended *)con)->qxmin+((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qtl,
		((queryContexExtended *)con)->qxmin,((queryContexExtended *)con)->qtl,((queryContexExtended *)con)->qxmin,((queryContexExtended *)con)->qtl+((queryContexExtended *)con)->qte,1);
	
	//printf("L2");
	
	count+=  findInterSections(((queryContexExtended *)con)->regionL2,
		((queryContexExtended *)con)->qxmax-((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qtl,((queryContexExtended *)con)->qxmax,((queryContexExtended *)con)->qtl,
		((queryContexExtended *)con)->qxmax,((queryContexExtended *)con)->qtl,((queryContexExtended *)con)->qxmax,((queryContexExtended *)con)->qtl+((queryContexExtended *)con)->qte,1);
	
	//y,t
	//printf("L3");
	
	count+=  findInterSections(((queryContexExtended *)con)->regionL3,
		((queryContexExtended *)con)->qymin,((queryContexExtended *)con)->qtl,((queryContexExtended *)con)->qymin+((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qtl,
		((queryContexExtended *)con)->qymin,((queryContexExtended *)con)->qtl,((queryContexExtended *)con)->qymin,((queryContexExtended *)con)->qtl+((queryContexExtended *)con)->qte,3);
	
	//printf("L4");
	
	count+=  findInterSections(((queryContexExtended *)con)->regionL4,
		((queryContexExtended *)con)->qymax-((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qtl,((queryContexExtended *)con)->qymax,((queryContexExtended *)con)->qtl,
		((queryContexExtended *)con)->qymax,((queryContexExtended *)con)->qtl,((queryContexExtended *)con)->qymax,((queryContexExtended *)con)->qtl+((queryContexExtended *)con)->qte,3);
	
	//x,t
	//printf("U1");
	
	count+=  findInterSections(((queryContexExtended *)con)->regionU1,
		((queryContexExtended *)con)->qxmin,((queryContexExtended *)con)->qth-((queryContexExtended *)con)->qte,((queryContexExtended *)con)->qxmin,((queryContexExtended *)con)->qth,
		((queryContexExtended *)con)->qxmin,((queryContexExtended *)con)->qth,((queryContexExtended *)con)->qxmin+((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qth,1);
	
	//printf("U2");
	
	count+=  findInterSections(((queryContexExtended *)con)->regionU2,
		((queryContexExtended *)con)->qxmax,((queryContexExtended *)con)->qth-((queryContexExtended *)con)->qte,((queryContexExtended *)con)->qxmax,((queryContexExtended *)con)->qth,
		((queryContexExtended *)con)->qxmax-((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qth,((queryContexExtended *)con)->qxmax,((queryContexExtended *)con)->qth,1);
	
	//y,t
	//printf("U3");
	
	count+=  findInterSections(((queryContexExtended *)con)->regionU3,
		((queryContexExtended *)con)->qymin,((queryContexExtended *)con)->qth-((queryContexExtended *)con)->qte,((queryContexExtended *)con)->qymin,((queryContexExtended *)con)->qth,
		((queryContexExtended *)con)->qymin,((queryContexExtended *)con)->qth,((queryContexExtended *)con)->qymin+((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qth,3);
	
	//printf("U4");
	
	count+=  findInterSections(((queryContexExtended *)con)->regionU4,
		((queryContexExtended *)con)->qymax,((queryContexExtended *)con)->qth-((queryContexExtended *)con)->qte,((queryContexExtended *)con)->qymax,((queryContexExtended *)con)->qth,
		((queryContexExtended *)con)->qymax-((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qth,((queryContexExtended *)con)->qymax,((queryContexExtended *)con)->qth,3);
	
	//x,y
	//printf("C1");
	
	count+=  findInterSections(((queryContexExtended *)con)->regionC1,
		((queryContexExtended *)con)->qxmin,((queryContexExtended *)con)->qymin,((queryContexExtended *)con)->qxmin+((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qymin,
		((queryContexExtended *)con)->qxmin,((queryContexExtended *)con)->qymin,((queryContexExtended *)con)->qxmin,((queryContexExtended *)con)->qymin+((queryContexExtended *)con)->qye,2);
	
	//printf("C2");
	
	count+=  findInterSections(((queryContexExtended *)con)->regionC2,
		((queryContexExtended *)con)->qxmin,((queryContexExtended *)con)->qymax-((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qxmin,((queryContexExtended *)con)->qymax,
		((queryContexExtended *)con)->qxmin,((queryContexExtended *)con)->qymax,((queryContexExtended *)con)->qxmin+((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qymax,2); //tkuznets
	
	//x,y
	//printf("C3");
	
	count+=  findInterSections(((queryContexExtended *)con)->regionC3,
		((queryContexExtended *)con)->qxmax,((queryContexExtended *)con)->qymax-((queryContexExtended *)con)->qye,((queryContexExtended *)con)->qxmax,((queryContexExtended *)con)->qymax,
		((queryContexExtended *)con)->qxmax-((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qymax,((queryContexExtended *)con)->qxmax,((queryContexExtended *)con)->qymax,2);
	 

	//printf("C4");
	
	count+=  findInterSections(((queryContexExtended *)con)->regionC4,
		((queryContexExtended *)con)->qxmax-((queryContexExtended *)con)->qxe,((queryContexExtended *)con)->qymin,((queryContexExtended *)con)->qxmax,((queryContexExtended *)con)->qymin,
		((queryContexExtended *)con)->qxmax,((queryContexExtended *)con)->qymin,((queryContexExtended *)con)->qxmax,((queryContexExtended *)con)->qymin+((queryContexExtended *)con)->qye,2);
    
	//cout<<"refine_exteneded_query_result - count: "<<count<<endl;
	return count;
}
//adopted from 
//http://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/
bool onSegment(Point p, Point q, Point r)
{
    if (q.x <= max(p.x, r.x) && q.x >= min(p.x, r.x) &&
        q.y <= max(p.y, r.y) && q.y >= min(p.y, r.y))
       return true;
 
    return false;
}
 
// To find orientation of ordered triplet (p, q, r).
// The function returns following values
// 0 --> p, q and r are colinear
// 1 --> Clockwise
// 2 --> Counterclockwise
int orientation(Point p, Point q, Point r)
{
    // See 10th slides from following link for derivation of the formula
    // http://www.dcs.gla.ac.uk/~pat/52233/slides/Geometry1x1.pdf
    int val = (q.y - p.y) * (r.x - q.x) -
              (q.x - p.x) * (r.y - q.y);
 
    if (val == 0) return 0;  // colinear
 
    return (val > 0)? 1: 2; // clock or counterclock wise
}
bool lineSegmentsIntersects ( double line1x1,double line1y1,double line1x2,double line1y2,double line2x1,double line2y1,double line2x2,double line2y2){
	Point p1= {line1x1,line1y1};
	Point q1 ={line1x2,line1y2};
	Point p2= {line2x1,line2y1};
	Point q2 ={line2x2,line2y2};
	int o1 = orientation(p1, q1, p2);
    int o2 = orientation(p1, q1, q2);
    int o3 = orientation(p2, q2, p1);
    int o4 = orientation(p2, q2, q1);
 
    // General case
    if (o1 != o2 && o3 != o4)
        return true;
 
    // Special Cases
    // p1, q1 and p2 are colinear and p2 lies on segment p1q1
    if (o1 == 0 && onSegment(p1, p2, q1)) return true;
 
    // p1, q1 and p2 are colinear and q2 lies on segment p1q1
    if (o2 == 0 && onSegment(p1, q2, q1)) return true;
 
    // p2, q2 and p1 are colinear and p1 lies on segment p2q2
    if (o3 == 0 && onSegment(p2, p1, q2)) return true;
 
     // p2, q2 and q1 are colinear and q1 lies on segment p2q2
    if (o4 == 0 && onSegment(p2, q1, q2)) return true;
 
    return false; // Doesn't fall in any of the above cases
}
//direction 1==x,t
//direction 2==x,y
//direction 3==y,t
int findInterSections(vector<resultNode *> * sortedList, double line1x1,double line1y1,double line1x2,double line1y2,
	double line2x1,double line2y1,double line2x2,double line2y2,int direction){
	int count=0;
	
	if ( sortedList->size() > 0) {
		

		for(int i=0;i < sortedList->size()-1; i++){
			//printf("sortedList->size() %d \n", sortedList->size());
			if((((*sortedList)[i])->oid==((*sortedList)[i+1])->oid)&&(((*sortedList)[i])->te==((*sortedList)[i+1])->ts)){
				if(direction==1 && 
					lineSegmentsIntersects(line1x1,line1y1,line1x2,line1y2,((*sortedList)[i])->x,((*sortedList)[i])->ts,((*sortedList)[i+1])->x,((*sortedList)[i+1])->ts)&&
					lineSegmentsIntersects(line2x1,line2y1,line2x2,line2y2,((*sortedList)[i])->x,((*sortedList)[i])->ts,((*sortedList)[i+1])->x,((*sortedList)[i+1])->ts))
					count++;
				else if (direction==2 && 
					lineSegmentsIntersects(line1x1,line1y1,line1x2,line1y2,((*sortedList)[i])->x,((*sortedList)[i])->y,((*sortedList)[i+1])->x,((*sortedList)[i+1])->y)&&
					lineSegmentsIntersects(line2x1,line2y1,line2x2,line2y2,((*sortedList)[i])->x,((*sortedList)[i])->y,((*sortedList)[i+1])->x,((*sortedList)[i+1])->y)) // tkuznets
					count++;
				else if(direction==3 && // tkuznets
					lineSegmentsIntersects(line1x1,line1y1,line1x2,line1y2,((*sortedList)[i])->y,((*sortedList)[i])->ts,((*sortedList)[i+1])->y,((*sortedList)[i+1])->ts)&&
					lineSegmentsIntersects(line2x1,line2y1,line2x2,line2y2,((*sortedList)[i])->y,((*sortedList)[i])->ts,((*sortedList)[i+1])->y,((*sortedList)[i+1])->ts))
					count++;
				//printf("count %d \n", count);
			}
		}
		//printf("count %d \n", count);
	}
	return count;
}
double query_spatial_time_slice_extended(double xmin, double ymin ,double xmax, double ymax, double tl, double th, void * con,double queryXE,double queryYE,double queryTE){
	  clock_t start, finish;
     
	  double  duration = 0;
	  
	  //printf("xmin %f ymin %f xmax %f ymax %f tl %f th %f qXE %f qYE %f qTE %f", xmin, ymin, xmax, ymax, tl, th, queryXE, queryYE, queryTE);
	  
	  Vector3D searchMin3D = Vector3D(xmin,ymin,tl);
	  Vector3D searchMax3D = Vector3D(xmax,ymax,th);
	  
	  start = clock();
	  int found  = tree.Search(searchMin3D.v, searchMax3D.v, &QueryResultCallbackExtended, con);

	  int refinedResultCount = refine_exteneded_query_result(xmin,ymin,xmax,ymax,tl,th,con,queryXE,queryYE,queryTE);
	  finish = clock();
	  
	  //printf("refine_exteneded_query_result: %0.8f \n", (double)(finish-start));
	  
	  retrievedResults += refinedResultCount;
	  
	  // Print results
	  //printf("\nResults!!!\n");
	  //cout<<"Result of query: "<<found<<endl;
	  //cout<<"Result of extended query: "<<refinedResultCount<<endl;
	  
	  duration = (double)(finish-start);
	  freeResultListExtended((queryContexExtended *)con); //tkuznets
	  delete con;
	  return duration;
}

double query_spatio_temporal_range(double xmin, double ymin, double xmax, double ymax, double tl, double th){
//	retrievedResults =0;
	queryContex * con  = new queryContex;

	con->resultList = new vector<resultNode*>;

	con->totalQueryResult=con->totalCorrectQueryResult =0;

	con->queryType = QUERY_TYPE;	
	con->qtl =tl;
	con->qth =th;
	con->qxmin = xmin ;
	con->qymin = ymin;
	con->qxmax = xmax;
	con->qymax = ymax;

	return query_spatial_time_slice( con->qxmin,  con->qymin , con->qxmax,  con->qymax,  con->qtl,  con->qth, con,false);
}
double query_spatio_temporal_range_extended(double xmin, double ymin, double xmax, double ymax, double tl, double th,double queryXE,double queryYE, double queryTE){
//	retrievedResults =0;
	queryContexExtended * con  = new queryContexExtended;

	// < tkuznets	
	con->regionL1 = new vector<resultNode*>;
	con->regionL2 = new vector<resultNode*>;
	con->regionL3 = new vector<resultNode*>;
	con->regionL4 = new vector<resultNode*>;
	con->regionC1 = new vector<resultNode*>;
	con->regionC2 = new vector<resultNode*>;
	con->regionC3 = new vector<resultNode*>;
	con->regionC4 = new vector<resultNode*>;
	con->regionU1 = new vector<resultNode*>;
	con->regionU2 = new vector<resultNode*>;
	con->regionU3 = new vector<resultNode*>;
	con->regionU4 = new vector<resultNode*>;
	// >

	con->totalQueryResult = con->totalCorrectQueryResult = 0;
	con->queryType = QUERY_TYPE;	
	
	
	con->qtl =tl;
	con->qth =th;
	
	con->qxmin = xmin ;
	con->qymin = ymin ;

	con->qxmax = xmax ;
	con->qymax = xmax ;
	con->qxe = queryXE;
	con->qye = queryYE;
	con->qte = queryTE;

	return query_spatial_time_slice_extended( xmin-queryXE,  ymin-queryYE , xmax+queryXE,  ymax+queryYE ,  tl-queryTE,  th+queryTE, con,queryXE,queryYE,queryTE); // tkuznets
}
void CountReset(){
	tree.CountReset();
	retrievedResults =0;
}
void  ShowStats(){
	tree.ShowStats();
}
void RemoveAll(){
	tree.RemoveAll();
}
int Count(int* currentcount){
	return  tree.Count(currentcount);
}

double getCurrentTimeStem(){
	return tree.currentTimeStamp;
}

};
#endif 