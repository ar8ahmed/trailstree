//header file for the update memo
//Xiaopeng Oct 2004
//Ahmed Mahmood 2013

#ifndef XXX_MDM_H
#define XXX_MDM_H

#include "parameters.h"
#include "LocEntry.h"

#define MAXHASH 10000
#include <windows.h>
#include <time.h>

static int everInsertedMDMEnt = 0;
static int currentMDMEntNum = 0;

struct MDMOIDEntry{
	
	int  order; //to ensure proper ordering of trajectory updates even if they appear to arrive at the same time 
	double startTimeStamp;
	MDMOIDEntry* NextEntry;
	MDMOIDEntry* PrevEntry;
};

struct MDMEntry2D
{
	int oid;
	
	int MDN; // to keep track of how many entries in the memo per object id
	int order; //to ensure proper ordering of trajectory updates even if they appear to arrive at the same time 
	MDMEntry2D* NextHash;
	MDMEntry2D* PrevHash;
	MDMOIDEntry* Head;
	MDMOIDEntry* Tail;
};

//in-memory linked list
class MDM2D
{
public:			
	int size;	
	int TotalCount;

public:				

	MDMEntry2D* HashHead[MAXHASH];
	MDMEntry2D* HashTail[MAXHASH];

	//AR this is the first entry for an objectID
	MDMEntry2D *InsertHash(int oid, int order,double startTime, int mdn = 1)  //insert a new MDM entry with MDN
	{
		size++;
		everInsertedMDMEnt++;
		currentMDMEntNum++;
		TotalCount = TotalCount+mdn;

		//Build a new Hash entry first
		MDMEntry2D *NewEntry = new MDMEntry2D;
		NewEntry->oid = oid;
		NewEntry->MDN = mdn;
		NewEntry->order = order;
		//build a new MDMoidentry
		MDMOIDEntry *newOIDEntry  = new MDMOIDEntry;
		newOIDEntry->order = order;
		newOIDEntry->startTimeStamp = startTime;
		
		newOIDEntry->NextEntry= NULL;
		newOIDEntry->PrevEntry= NULL;
		//initialize the first entry in the memo list 
		NewEntry->Head= newOIDEntry;
		NewEntry->Tail= newOIDEntry;


		int slot = oid%MAXHASH; 

		//Insert in the hash table structure	
		if(HashHead[slot] == NULL)
		{
			NewEntry->NextHash = NULL;
			NewEntry->PrevHash = NULL;
			HashHead[slot] = NewEntry;
			HashTail[slot] = NewEntry;
			return NewEntry;
		}

		MDMEntry2D* pos = HashHead[slot];
		while(pos && (pos->oid > NewEntry->oid))
		{
			pos = pos->NextHash;
		}

		if(pos == HashHead[slot])  //insert before head
		{
			HashHead[slot]->PrevHash = NewEntry;
			NewEntry->NextHash = HashHead[slot];
			NewEntry->PrevHash = NULL;
			HashHead[slot] = NewEntry;
		}
		else if(pos == NULL) //insert after tail
		{
			HashTail[slot]->NextHash = NewEntry;
			NewEntry->PrevHash = HashTail[slot];
			NewEntry->NextHash = NULL;
			HashTail[slot] = NewEntry;
		}
		else  //insert in middle
		{
			NewEntry->NextHash = pos;
			NewEntry->PrevHash = pos->PrevHash;
			pos->PrevHash->NextHash = NewEntry;
			pos->PrevHash = NewEntry;
		}

		return NewEntry;
	}

	
	MDMEntry2D *SearchOid(int o)
	{
		int slot = o%MAXHASH;
		MDMEntry2D *temp = HashHead[slot];
		while (temp!=NULL)
		{
			if (temp->oid==o)
				return temp;
			if(temp->oid < o)
				return NULL;
			temp = temp->NextHash;
		}
		return NULL;
	}

	

	void DeleteHash(MDMEntry2D *DelRec)
	{
		size--;
		currentMDMEntNum--;
		TotalCount = TotalCount - DelRec->MDN;
		
		//first remove all entries for this OID
		MDMOIDEntry * p = DelRec->Head;
		MDMOIDEntry * p1;
		while(p!=NULL){
			p1 = p;
			p=p->NextEntry;
			delete p1; 
		}
		DelRec->Head = NULL;
		DelRec->Tail = NULL;

		

		int slot = DelRec->oid%MAXHASH;

		if (DelRec->NextHash != NULL)
			DelRec->NextHash->PrevHash = DelRec->PrevHash;				
		if (DelRec->PrevHash != NULL)
			DelRec->PrevHash->NextHash = DelRec->NextHash;				

		if(HashHead[slot]==DelRec)
		{
			if(DelRec->NextHash != NULL)
				HashHead[slot] = DelRec->NextHash;
			else
				HashHead[slot] = NULL;
		}

		if(HashTail[slot]==DelRec)
		{
			if(DelRec->PrevHash != NULL)
				HashTail[slot] = DelRec->PrevHash;
			else
				HashTail[slot] = NULL;
		}
		delete DelRec;
	}

	void ChangeMDN(MDMEntry2D *DelRec, int delta) //change the MDN of the MDM entry by delta, if MDN==0, delete the MDM entry
	{
		DelRec->MDN = DelRec->MDN + delta;
		TotalCount = TotalCount + delta;
		if(DelRec->MDN == 0)
		{
			DeleteHash(DelRec);
		}
	}

	// returns the end time of an entry and -1 if the entry is current 
	// this entry is now cleaned and it shoudl eb removed from the MEMO
	double getEndTimeRemoveUpdate(MDMEntry2D *OriRec, int order){
		MDMOIDEntry * p = OriRec->Head;
		double endtime =-1;
		while(p!=NULL&&p->order<=order)
			p=p->NextEntry;
		if(p==NULL)
			return -1;
		else{ // remove this entry from the memo it is no longer needed 
			endtime = p->startTimeStamp;
			if(p->NextEntry ==NULL&&p->PrevEntry ==NULL) { //this is the first entry 
				OriRec->Head = NULL;
				OriRec->Tail = NULL;
				delete p;

			}
			else if(p->NextEntry ==NULL){// last entry in the list 
				p->PrevEntry->NextEntry=NULL;
				OriRec->Tail = p->PrevEntry;	
				delete p;
			}
			else if(p->PrevEntry ==NULL){//first entry in the list 
				p->NextEntry->PrevEntry=NULL;
				OriRec->Head= p->NextEntry;	
				delete p;
			}
			else {//deletion at middle
				p->NextEntry ->PrevEntry = p->PrevEntry;
				p->PrevEntry ->NextEntry =p->NextEntry;	
				delete p;
			}
		}
		return endtime;
	}
	//This function ensures no phanom entries in the memo (i.e., no expired entries)
	//whenever a time stamp expires it gets removed from the memo
	void removeExpiredTimeStampIfExisits(MDMEntry2D *OriRec, int order){
		MDMOIDEntry * p = OriRec->Head;
		double endtime =-1;
		while(p!=NULL&&p->order<order)
			p=p->NextEntry;
		if(p==NULL||p->order!=order)
			return ;
		else { // remove this entry from the memo it is no longer needed 
			endtime = p->startTimeStamp;
			if(p->NextEntry ==NULL&&p->PrevEntry ==NULL) { //this is the first entry 
				OriRec->Head = NULL;
				OriRec->Tail = NULL;
				delete p;

			}
			else if(p->NextEntry ==NULL){// last entry in the list 
				p->PrevEntry->NextEntry=NULL;
				OriRec->Tail = p->PrevEntry;	
				delete p;
			}
			else if(p->PrevEntry ==NULL){//first entry in the list 
				p->NextEntry->PrevEntry=NULL;
				OriRec->Head= p->NextEntry;	
				delete p;
			}
			else {//deletion at middle
				p->NextEntry ->PrevEntry = p->PrevEntry;
				p->PrevEntry ->NextEntry =p->NextEntry;	
				delete p;
			}
		}
	
	}
	// returns the end time of an entry and -1 if the entry is current for search purpose only and doesnt remove the entry 
	double getEndTime(MDMEntry2D *OriRec, int order){
		MDMOIDEntry * p = OriRec->Head;
		double endtime =-1;
		while(p!=NULL&&p->order<=order)
			p=p->NextEntry;
		if(p==NULL)
			return -1;
		else{ 
			endtime = p->startTimeStamp;
		}
		return endtime;
	}
	void ChangeMDNOnly( MDMEntry2D *DelRec, int delta) //change the MDN of the MDM entry by delta only w/o delete the MDM entry
	{
		DelRec->MDN = DelRec->MDN + delta;
		TotalCount = TotalCount + delta;
	}

	void UpdateHash(MDMEntry2D *OriRec, int order) //update the order information of the MDM entry
	{
		OriRec->order = order;
	}
	void UpdateHash(MDMEntry2D *OriRec, int order,double timeStart) //AR update the order information time strat of the MDM entry
	{
		OriRec->order = order;
		
		//this is an append only operation so a new entry will be inserted in the last 
		MDMOIDEntry * newEntry = new MDMOIDEntry;
		
		newEntry->startTimeStamp = timeStart;
		newEntry->order = order;
		newEntry->NextEntry = NULL;
		newEntry->PrevEntry = NULL;


		//first check if this is the first item 

		if(OriRec->Tail==NULL ||  OriRec->Head==NULL)
			OriRec->Tail=OriRec->Head = newEntry;
		else{
			MDMOIDEntry * p = OriRec->Tail;
			newEntry->NextEntry = p->NextEntry;
			p->NextEntry = newEntry;
			newEntry->PrevEntry =p;
			OriRec->Tail = newEntry;
		}
	
			
		
	}
	void deleteAllOIDentries(MDMEntry2D *DelRec){
		MDMOIDEntry * p = DelRec->Head;
		MDMOIDEntry * p1;
		while(p!=NULL){
			p1 = p;
			p=p->NextEntry;
			delete p1; 
		}
		DelRec->Head = NULL;
		DelRec->Tail = NULL;
	}
	void deleteAllEntries()
	{
		MDMEntry2D *Temp1, *Temp2;

		for(int i=0; i<MAXHASH; i++)
		{
			Temp1 = HashHead[i];
			while (Temp1!=NULL)
			{ 
				Temp2 = Temp1;
				Temp1 = Temp1->NextHash;
				deleteAllOIDentries(Temp2);
				delete Temp2;
				currentMDMEntNum--;
			}
			
			HashHead[i] = NULL;
			HashTail[i] = NULL;
		}
		
		size = 0;
		currentMDMEntNum = 0;
		TotalCount = 0;
	}

	MDM2D()
	{
		size = 0;
		TotalCount = 0;
		for(int i=0; i<MAXHASH; i++)
		{
			HashHead[i] = NULL;
			HashTail[i] = NULL;
		}
	}

	~MDM2D()
	{
		MDMEntry2D *Temp1, *Temp2;

		for(int i=0; i<MAXHASH; i++)
		{
			Temp1 = HashHead[i];
			while (Temp1!=NULL)
			{ 
				Temp2 = Temp1;
				Temp1 = Temp1->NextHash;
				deleteAllOIDentries(Temp2);
				delete Temp2;
				size--;
				currentMDMEntNum--;
			}
		}
	}
};

#endif
