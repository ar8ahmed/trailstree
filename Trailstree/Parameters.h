#ifndef PARAMETERS_H
#define PARAMETERS_H

// NOTE This file compiles under MSVC 6 SP5 and MSVC .Net 2003 it may not work on other compilers without modification.

// Parameters for R*-tree
#define BRANCHPERNODE	340//number of branches per node. //1024=>42 ,2048=>84 ,4096=>170, 8192=> [340] (RUM)
                                                          //1024=>50 ,2048=>102 ,4096=>204, 8192=>409 (FUR and R*) 
#define MAXENTRYTOCHECK ((int)(BRANCHPERNODE*0.64))  //maximum number of entries to check when choose subtree
#define REINSERTPERCENT 0.05 //the percentage of entries in a node that is to be re-inserted
#define MINNODERATIO 0.4

// Parameters for moving objects
//AR #define NUM_OBJECTS  (1000000) // Number of objects in test set. The input data can have more tuples. [1'],2',5',10'
							 //NUM_OBJECTS should be always multiple of (500,000) or  a value < 500,000

#define MOVINGPERC (1) //moving percentage of NUM_OBJECTS. NOT USED
#define CYCLENUM 1  //number of updating cycles. NOT USED
#define MAX_WORLDSIZE (31000.0f)//10000.0f  31000.0f
#define MAX_WORLDSIZE_X (31000.0f)//10000.0f  31000.0f
#define MAX_WORLDSIZE_Y (31000.0f)//10000.0f  31000.0f
#define FRAC_WORLDSIZE (MAX_WORLDSIZE*0)//OBJECT EXTENT. [0], 0.002,0.004,0.006,0.008,0.010 
#define MOV_WORLDSIZE (MAX_WORLDSIZE*0.04)//MOVING DISTANCE. 0.001, 0.01, 0.02, [0.04], 0.06, 0.08, 0.10  !!!!
#define MOV_DIST_RATIO 0.01//MOVING DISTANCE. 0.001, 0.01, 0.02, [0.04], 0.06, 0.08, 0.10  !!!!

//Parameters for LDR-tree
#define ALWAYS 0  //old entries are purged whenever a node is accessed
#define CLEANWHILEINSERT 0   //whether clean old entries when touch a page in insert or update ***ALWAYS
#define CLEANINGTOKENS 0 //cleaning tokens ***. If cleaning tokens is used alone
#define LRCLIST	1		//list of the least recent cleaned nodes *** If LRC and cleaning tokens is used.
#define NUMUPDATESPERTOKEN 400//give tokens after how many updates. INSPECTION INTERVAL. 2000 (1%), 400(5%),[200](10%), 100(20%), 40(50%), 20(100%)
#define NUMTOKENS 20  //number of tokens
#define CLEANOPTION CLEANINGTOKENS//LRCLIST   //clean option. 1
//there is not parameter to control inspection threshold T. Value is 0
//only 1 of CLEANINGTOKENS, LRCLIST can be set to '1'
 
//Parameters for queries
#define NUM_QUERIES (1000) //Number of queries in test set 100000
#define QUERY_TYPE (2) //0,1,2,3,4,5,6,7, ..8 is for random query type
#define QUERYEXTENT (.06)//this value is the size of each side of the square *** 0.02, 0.04, [0.06], 0.08, 0.10
#define QUIRY_WHILE_UPDATING (0) // to indicate if you should query doiung updates of after it 
//#define QUERY_TIME_SPAN (50)
//New parameters


#define TEST_INSERT_ALONE 0 
#define FRACTION_UPDATES 0  //0=no filter. 1=filter updates in downtown and rest of the map ****
#define PERC_UPDATES_DOWNTOWN (100)//SEE PREVIOUS ****
#define PERC_UPDATES_REST (10)//SEE PREVIOUS ****
#define DATABASE_TYPE 1//1= Network 50, 2 = DSO, 3=Network 10,4=Network 150, 5=Network 250, (6=Network 200, 7=Network 100)****
#define OBJECTS_PER_DATFILE 500000//Number of objects in each datafile. 500000 ****
#define SPATIAL_SCALE 1


//parameters for sliding window

//#define MAX_NUMBER_UPDATES (1000000) //NUMBER OF UPDATES IN TOTAL. Observe that there are 10 updates per object in datafiles.[1'],2',5',10'

#define DEFAULT_DURATION 20000
#define NOW_TIME 1000000000
#define R_TREE 1// 0 regular R* tree 
#define NODE_SPLIT 0// 0 regular R* tree , 1 Temporal split, 2 trajectory split

///  setup test 1

//#define NUM_OBJECTS  (100000)


// berlinMOD setup test 2
//#define MAX_WORLDSIZE_X (64408)//10000.0f  31000.0f
//#define MAX_WORLDSIZE_y (34781)//10000.0f  31000.0f
#define QUERYEXTENT (.06)//this value is the size of each side of the square *** 0.005, 0.01, 0.04, 0.6, 0.10, .30
#define QUERY_TIME_SPAN (.3)


#define QUERY_OUT_FILE "E:\\work\\purdue\\database\\research\\datasets\\Geolife Trajectories 1.3\\RUMQueryResult.txt"


/// berlinMOD setup test 3
//#define MAX_WORLDSIZE_X (64408)//10000.0f  31000.0f
//#define MAX_WORLDSIZE_y (34781)//10000.0f  31000.0f
//#define NUM_OBJECTS  (10001)


#endif
//TESTING CONFIGURATION:
//FOR DATABASE SIZE
// 1,000,000
//#define NUM_OBJECTS  (1000000)
//#define MAX_NUMBER_UPDATES (10000000)//up to 10 times size, can be less
// 2,000,000
//#define NUM_OBJECTS  (2000000)
//#define MAX_NUMBER_UPDATES (20000000)
// 4,000,000
//#define NUM_OBJECTS  (4000000)
//#define MAX_NUMBER_UPDATES (40000000)
// 6,000,000
//#define NUM_OBJECTS  (6000000)
//#define MAX_NUMBER_UPDATES (60000000)
// 8,000,000
//#define NUM_OBJECTS  (8000000)
//#define MAX_NUMBER_UPDATES (80000000)
// 10,000,000
//#define NUM_OBJECTS  (10000000)
//#define MAX_NUMBER_UPDATES (100000000)

//FOR CLEANING APPROACH
//==> 1 Cleaning tokens
//#define CLEANINGTOKENS 1 
//#define LRCLIST	0		
//#define ALWAYS 0

//==> 2 Clean Upon Taouch + Cleaning tokens
//#define CLEANINGTOKENS 1 
//#define LRCLIST	0		
//#define ALWAYS 1

//==> 3 LRC + Clean Upon Taouch + Cleaning tokens
//#define CLEANINGTOKENS 0 
//#define LRCLIST	1		
//#define ALWAYS 1
#define SPATIAL_STEP 4 
#define X_Range 10000
#define Y_Range 10000
#define D_Range 200


#define MAX_WORLDSIZE_X (10000)//10000.0f  31000.0f
#define MAX_WORLDSIZE_Y (10000)//10000.0f  31000.0f

//--------------------------------------------------------
//---Uniform dataset -----------------------------------
/*
#define NUM_OBJECTS (25001) //10001 [25001] 50001
#define MAX_NUMBER_UPDATES ((NUM_OBJECTS-1)*100) //NUMBER OF UPDATES IN TOTAL. Observe that there are 10 updates per object in datafiles.[1'],2',5',10'
#define DEFAULT_DURATION 20000 //10000 // [20000] // 40000
#define R_TREE 1
#define X_Range 10000
#define Y_Range 10000
#define D_Range DEFAULT_DURATION
*/


//--------------------------------------------------------
//---brinkhoff dataset -----------------------------------
/*
#define DEFAULT_DURATION 20000 //10000 // [20000] // 40000
#define NUM_OBJECTS (10001)   //4001 // [10001] // 20001
#define MAX_NUMBER_UPDATES (2500000) //1000000    //[2500000] // 5000000 //NUMBER OF UPDATES IN TOTAL.
#define R_TREE 1
#define X_Range 10000
#define Y_Range 10000
#define D_Range DEFAULT_DURATION
*/

//--------------------------------------------------------
//---Geolife dataset -----------------------------------
/*
#define DEFAULT_DURATION 6000000   // 3000000 [6000000] 12000000
#define NUM_OBJECTS (18690)
#define MAX_NUMBER_UPDATES (2500000)  //1000000  //[2500000] // 5000000 //NUMBER OF UPDATES IN TOTAL.
#define R_TREE 1
#define X_Range 10000
#define Y_Range 10000
#define D_Range DEFAULT_DURATION
*/
//--------------------------------------------------------
//---T-Drive dataset -----------------------------------
/*
#define DEFAULT_DURATION 20000  //10000 // [20000] // 40000
#define NUM_OBJECTS (10357)
#define MAX_NUMBER_UPDATES (2500000) //1000000    //[2500000] // 5000000 //NUMBER OF UPDATES IN TOTAL. 
#define R_TREE 1
#define X_Range 10000
#define Y_Range 10000
#define D_Range DEFAULT_DURATION
#define MAX_WORLDSIZE_X (10000)
#define MAX_WORLDSIZE_Y (10000)
*/
//--------------------------------------------------------
//---GSTD dataset -----------------------------------

#define DEFAULT_DURATION 100
#define NUM_OBJECTS (100000)
#define MAX_NUMBER_UPDATES (2500000) //1000000    //[2500000] // 5000000 //NUMBER OF UPDATES IN TOTAL. 
#define R_TREE 1
#define X_Range 10000
#define Y_Range 10000
#define D_Range DEFAULT_DURATION
#define MAX_WORLDSIZE_X (10000)
#define MAX_WORLDSIZE_Y (10000)
