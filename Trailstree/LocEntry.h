//header file for object location entry class (oid, xloc, yloc) for 2D case and (oid, xloc, yloc, zloc) for 3D case
//Xiaopeng Jan 2005
//Ahmed Mahmoo 2013

#ifndef XXX_LOCENTRY_H
#define XXX_LOCENTRY_H

#include "parameters.h"
#include <time.h>
#include <windows.h>
/// Simplify handling of 2 dimensional coordinate

//----------------------------------------------------------
//------ AR 3 D for limited time window 

/// Simplify handling of 3 dimensional coordinate
struct Vector3D
{
  ///< 3 double components for axes or dimensions
  double v[3];

  /// Default constructor
  Vector3D() {}

  /// Construct from four elements
  Vector3D(double a_x, double a_y, double a_z)
  {
    v[0] = a_x;
    v[1] = a_y;
	v[2] = a_z;
  }
  
  /// Add two vectors and return result
  Vector3D operator+ (const Vector3D& a_other) const
  {
    return Vector3D(v[0] + a_other.v[0], 
                v[1] + a_other.v[1],
				v[2] + a_other.v[2]
				);
  }

  bool operator== (const Vector3D& a_other) const
  {
	  if((v[0] == a_other.v[0])&&(v[1] == a_other.v[1])&&(v[2] == a_other.v[2]))
		  return true;
	  return false;
  }
};

class LocEntry3D
{
public:
	int oid;
	Vector3D vmin, vmax;
	int order;
	//double start;
	//double  end;
	double duration;
	//bool recent;

	LocEntry3D(){};
	LocEntry3D(int o, Vector3D min, Vector3D max)
	{
		oid = o;
		vmin = min;
		vmax = max;
		//recent = true;
	};

	LocEntry3D(int o, Vector3D min, Vector3D max, int ord)
	{
		oid = o;
		vmin = min;
		vmax = max;
		order = ord;
		//recent = true;
	};
	/*
	LocEntry3D(int o, Vector3D min, Vector3D max, int ord, double st,double dur)
	{
		oid = o;
		vmin = min;
		vmax = max;
		order = ord;
		//start = st;
		duration = dur;
		//recent = true;
	};
	*/
	bool ContentEqual(const LocEntry3D& loc_other) const
	{
	  if((oid == loc_other.oid)&&(vmin == loc_other.vmin)&&(vmax == loc_other.vmax))
		  return true;
	  return false;
	}

	bool operator== (const LocEntry3D& loc_other) const
	{
	  if((oid == loc_other.oid)&&(vmin == loc_other.vmin)&&(vmax == loc_other.vmax)&&(order == loc_other.order)/*&&(start == loc_other.start)*/&&((duration - loc_other.duration)<0.0000001))
		  return true;
	  return false;
	}

	bool operator!= (const LocEntry3D& loc_other) const
	{
	  if((oid == loc_other.oid)&&(vmin == loc_other.vmin)&&(vmax == loc_other.vmax)&&(order == loc_other.order)/*&&(start == loc_other.start)*/&&((duration - loc_other.duration)<0.0000001))
		  return true;
	  return false;
	}

	~LocEntry3D() {};
};

#endif