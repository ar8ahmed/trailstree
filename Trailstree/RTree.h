#pragma once
#ifndef RTREE_H
#define RTREE_H

// NOTE This file compiles under MSVC 6 SP5 and MSVC .Net 2003 it may not work on other compilers without modification.

// NOTE These next few lines may be win32 specific, you may need to modify them to compile on other platform
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <stdlib.h>
//#include <double.h>
#include <time.h>
#include "Parameters.h"
#include "MDM.h"
#include "nodelist.h"
#include "LocEntry.h"
#include <iomanip>
using namespace std;

#define ASSERT assert // RTree uses ASSERT( condition )
#ifndef Min
  #define Min __min 
#endif //Min
#ifndef Max
  #define Max __max 
#endif //Max

//
// RTree.h
//

#define RTREE_TEMPLATE template<class DATATYPE, class ELEMTYPE, int NUMDIMS, class ELEMTYPEREAL, int TMAXNODES, int TMINNODES>
#define RTREE_QUAL RTree<DATATYPE, ELEMTYPE, NUMDIMS, ELEMTYPEREAL, TMAXNODES, TMINNODES>

#define RTREE_DONT_USE_MEMPOOLS // This version does not contain a fixed memory allocator, fill in lines with EXAMPLE to implement one.
//#define RTREE_USE_SPHERICAL_VOLUME // Better split classification, may be slower on some systems


enum ACTION {SPLIT, REINSERT, NONE}; //type of possible actions that are returned when insert a data entry


//temp structure used for qsort
struct EleEntry
{
	double field;
	int indexnum;
};



//temp structure used for qsort
struct EleEntryTraj
{
	int id;
	double ts;
	int indexnum;
};



/// \class RTree
/// Implementation of RTree, a multidimensional bounding rectangle tree.
/// Example usage: For a 3-dimensional tree use RTree<Object*, double, 3> myTree;
///
/// This modified, templated C++ version by Greg Douglas at Auran (http://www.auran.com)
///
/// DATATYPE Referenced data, should be int, void*, obj* etc. no larger than sizeof<void*> and simple type
/// ELEMTYPE Type of element such as int or double
/// NUMDIMS Number of dimensions such as 2 or 3
/// ELEMTYPEREAL Type of element that allows fractional and large values such as double or double, for use in volume calcs
///
/// NOTES: Inserting and removing data requires the knowledge of its constant Minimal Bounding Rectangle.
///        This version uses new/delete for nodes, I recommend using a fixed size allocator for efficiency.
///        Instead of using a callback function for returned results, I recommend an efficient pre-sized, grow-only memory
///        array similar to MFC CArray or STL Vector for returning search query result.
///
template<class DATATYPE, class ELEMTYPE, int NUMDIMS, 
         class ELEMTYPEREAL = ELEMTYPE, int TMAXNODES = BRANCHPERNODE, int TMINNODES = (int)(TMAXNODES * MINNODERATIO)>
class RTree
{
	
protected: 
	
	//a comparing function feed to qsort, compare two EleEntry.field and sort in INCREASING order
static int compareEleEntryInc(const void *arg1, const void *arg2)
{
	EleEntry* e1 = (EleEntry*)arg1;
	EleEntry* e2 = (EleEntry*)arg2;

	if(e1->field > e2->field)
		return 1; 

	if(e1->field < e2->field)
		return -1;
	
	return 0;
}
static int compareEleEntryTraj(const void *arg1, const void *arg2)
{
	EleEntryTraj* e1 = (EleEntryTraj*)arg1;
	EleEntryTraj* e2 = (EleEntryTraj*)arg2;

	if(e1->id > e2->id ||(e1->id == e2->id &&e1->ts>e2->ts))
		return 1; 
	else 
		return -1;
	
	
}

//a comparing function feed to qsort, compare two EleEntry.field and sort in DECREASING order
static int compareEleEntryDec(const void *arg1, const void *arg2)
{
	EleEntry* e1 = (EleEntry*)arg1;
	EleEntry* e2 = (EleEntry*)arg2;

	if(e1->field > e2->field)
		return -1; 

	if(e1->field < e2->field)
		return 1;
	
	return 0;
}
  struct Node;  // Fwd decl.  Used by other internal structs and iterator

public:

  // These constant must be declared after Branch and before Node struct
  // Stuck up here for MSVC 6 compiler.  NSVC .NET 2003 is much happier.
  enum
  {
    MAXNODES = TMAXNODES,                         ///< Max elements in node
    MINNODES = TMINNODES,                         ///< Min elements in node
  };


public:
	friend int compareType (const void * a, const void * b);
  RTree();
  virtual ~RTree();
  int globalCnt; //global monotonic incrementing count
  double currentTimeStamp;
  int numdExpired;

  int globalNID; //Node ID assigner
 
  int rootLevel; //the level the root is. E.g., rootLevel == 2 ---> tree has level 0 (leaf), 1 and 2
  int nodeRead, nodeWrite, leafNodeRead,leafNodeReadSearch, leafNodeWrite, leafNodeWriteDueToCleaning;
  int visitedNotFound;
  MDM2D* MDM;
  NodeList* nl;
  double cleanTime;
  double updateTime;

  ofstream queryOutfile;
  
 
  /// \param a_min Min of bounding rect
  /// \param a_max Max of bounding rect
  /// \param a_dataId Positive Id of data.  Maybe zero, but negative numbers not allowed.
  void Insert(const ELEMTYPE a_min[NUMDIMS], const ELEMTYPE a_max[NUMDIMS], const DATATYPE& a_dataId);


  /// \param a_min Min of bounding rect
  /// \param a_max Max of bounding rect
  /// \param a_dataId Positive Id of data.  Maybe zero, but negative numbers not allowed.
  void Remove(const ELEMTYPE a_min[NUMDIMS], const ELEMTYPE a_max[NUMDIMS], const DATATYPE& a_dataId);

  /// Insert a new object entry, called when an object registers itself into the system  
  /// Similar to the insert(). Difference: if an MDM entry 
  /// exists for the object, update its location information without incrementing its MDN.
  /// ****************************************************************************************************
  /// IMPORTANT: Consecutive Register() calls without a Drop() call are not supported.
  /// ****************************************************************************************************
  void Register(const ELEMTYPE a_min[NUMDIMS], const ELEMTYPE a_max[NUMDIMS], const DATATYPE& a_dataId);

  /// Remove an existing entry out of the system, called when an object disconnects itself from the system
  /// ****************************************************************************************************
  /// IMPORTANT: Consecutive Drop() calls without a Register() call are not supported.
  /// ****************************************************************************************************
  void Drop(const ELEMTYPE a_min[NUMDIMS], const ELEMTYPE a_max[NUMDIMS], const DATATYPE& a_dataId);

  /// Update an object entry, there should have already an entry for the object in the system
  /// Similar to the insert(). Difference: one more step: insert an MDM entry (OID, newloc, 1) or modify 
  /// an existing entry as (OID, newloc, MDN+1).
  void Update(const ELEMTYPE a_min[NUMDIMS], const ELEMTYPE a_max[NUMDIMS], const DATATYPE& a_dataId);  

  /// Find all within search rectangle
  /// \param a_min Min of search bounding rect
  /// \param a_max Max of search bounding rect
  /// \param a_searchResult Search result array.  Caller should set grow size. Function will reset, not append to array.
  /// \param a_resultCallback Callback function to return result.  Callback should return 'true' to continue searching
  /// \param a_context User context to pass as parameter to a_resultCallback
  /// \return Returns the number of entries found
  int Search(const ELEMTYPE a_min[NUMDIMS], const ELEMTYPE a_max[NUMDIMS], bool __cdecl a_resultCallback(DATATYPE a_data, void* a_context), void* a_context);

  /// Find all recent within search rectangle
  /// \param a_min Min of search bounding rect
  /// \param a_max Max of search bounding rect
  /// \param a_searchResult Search result array.  Caller should set grow size. Function will reset, not append to array.
  /// \param a_resultCallback Callback function to return result.  Callback should return 'true' to continue searching
  /// \param a_context User context to pass as parameter to a_resultCallback
  /// \return Returns the number of entries found
  int SearchRecent(const ELEMTYPE a_min[NUMDIMS], const ELEMTYPE a_max[NUMDIMS], bool __cdecl a_resultCallback(DATATYPE a_data, void* a_context), void* a_context);
  
  /// Remove all entries from tree
  void RemoveAll();

  /// Count the data elements in this container.  This is slow as no internal counter is maintained.
  int Count(int* currentcount);


  /// Reset nodeRead, nodeWrite, leafNodeRead, leafNodeWrite
  void CountReset();


  // Show the statistics of the tree
  void ShowStats();

  /// Iterator is not remove safe.
  class Iterator
  {
  private:
  
    enum { MAX_STACK = 32 }; //  Max stack size. Allows almost n^32 where n is number of branches in node
    
    struct StackElement
    {
      Node* m_node;
      int m_branchIndex;
    };
    
  public:
  
    Iterator()                                    { Init(); }

    ~Iterator()                                   { }
    
    /// Is iterator invalid
    bool IsNull()                                 { return (m_tos <= 0); }

    /// Is iterator pointing to valid data
    bool IsNotNull()                              { return (m_tos > 0); }

    /// Access the current data element. Caller must be sure iterator is not NULL first.
    DATATYPE& operator*()
    {
      ASSERT(IsNotNull());
      StackElement& curTos = m_stack[m_tos - 1];
      return curTos.m_node->m_branch[curTos.m_branchIndex].m_data;
    } 

    /// Access the current data element. Caller must be sure iterator is not NULL first.
    const DATATYPE& operator*() const
    {
      ASSERT(IsNotNull());
      StackElement& curTos = m_stack[m_tos - 1];
      return curTos.m_node->m_branch[curTos.m_branchIndex].m_data;
    } 

    /// Find the next data element
    bool operator++()                             { return FindNextData(); }

  private:
  
    /// Reset iterator
    void Init()                                   { m_tos = 0; }

    /// Find the next data element in the tree (For internal use only)
    bool FindNextData()
    {
      for(;;)
      {
        if(m_tos <= 0)
        {
          return false;
        }
        StackElement curTos = Pop(); // Copy stack top cause it may change as we use it

        if(curTos.m_node->IsLeaf())
        {
          // Keep walking through data while we can
          if(curTos.m_branchIndex+1 < curTos.m_node->m_count)
          {
            // There is more data, just point to the next one
            Push(curTos.m_node, curTos.m_branchIndex + 1);
            return true;
          }
          // No more data, so it will fall back to previous level
        }
        else
        { 
          if(curTos.m_branchIndex+1 < curTos.m_node->m_count)
          {
            // Push sibling on for future tree walk
            // This is the 'fall back' node when we finish with the current level
            Push(curTos.m_node, curTos.m_branchIndex + 1);
          }
          // Since cur node is not a leaf, push first of next level to get deeper into the tree
          Node* nextLevelnode = curTos.m_node->m_branch[curTos.m_branchIndex].m_child;
          Push(nextLevelnode, 0);
          
          // If we pushed on a new leaf, exit as the data is ready at TOS
          if(nextLevelnode->IsLeaf())
          {
            return true;
          }
        }
      }
    }

    /// Push node and branch onto iteration stack (For internal use only)
    void Push(Node* a_node, int a_branchIndex)
    {
      m_stack[m_tos].m_node = a_node;
      m_stack[m_tos].m_branchIndex = a_branchIndex;
      ++m_tos;
      ASSERT(m_tos <= MAX_STACK);
    }
    
    /// Pop element off iteration stack (For internal use only)
    StackElement& Pop()
    {
      ASSERT(m_tos > 0);
      --m_tos;
      return m_stack[m_tos];
    }

    StackElement m_stack[MAX_STACK];              ///< Stack as we are doing iteration instead of recursion
    int m_tos;                                    ///< Top Of Stack index
  
    friend RTree; // Allow hiding of non-public functions while allowing manipulation by logical owner
  };

  /// Get 'first' for iteration
  void GetFirst(Iterator& a_it)
  {
    a_it.Init();
    if(m_root && (m_root->m_count > 0))
    {
      a_it.Push(m_root, 0);
      a_it.FindNextData();
    }
  }

  /// Get Next for iteration
  void GetNext(Iterator& a_it)                    { ++a_it; }

  /// Is iterator NULL, or at end?
  bool IsNull(Iterator& a_it)                     { return a_it.IsNull(); }

  /// Get object at iterator position
  DATATYPE& GetAt(Iterator& a_it)                 { return *a_it; }

protected:

  /// Minimal bounding rectangle (n-dimensional)
  struct Rect
  {
    ELEMTYPE m_min[NUMDIMS];                      ///< Min dimensions of bounding box 
    ELEMTYPE m_max[NUMDIMS];                      ///< Max dimensions of bounding box 
  };


  /// May be data or may be another subtree
  /// The parents level determines this.
  /// If the parents level is 0, then this is data
  struct Branch
  {
    Rect m_rect;                                  ///< Bounds
	
	
    union
    {
      Node* m_child;                              ///< Child node
      DATATYPE m_data;                            ///< Data Id or Ptr
    };
  };

  /// Node for each branch level
  struct Node
  {
    bool IsInternalNode()                         { return (m_level > 0); } // Not a leaf, but a internal node
    bool IsLeaf()                                 { return (m_level == 0); } // A leaf, contains data
    
    int m_count;                                  ///< Count
    int m_level;                                  ///< Leaf is zero, others positive
	int m_nid;										/// node id

    Branch m_branch[MAXNODES];                    ///< Branch

  };
  
  /// A link list of nodes for reinsertion after a delete operation
  struct ListNode
  {
    ListNode* m_next;                             ///< Next in list
    Node* m_node;                                 ///< Node
  };

  /// A link list of branches for reinsertion after a delete/insert operation
  struct ListBranch
  {
    ListBranch* m_next;                             ///< Next in list
	int m_level;									//the level of the branch
    Branch* m_branch;                                 ///< Node
  };

  /// Variables for finding a split partition
  struct PartitionVars
  {
    int m_partition[MAXNODES+1];
    int m_total;
    int m_minFill;
    int m_taken[MAXNODES+1];
    int m_count[2];
    Rect m_cover[2];
    ELEMTYPEREAL m_area[2];

    Branch m_branchBuf[MAXNODES+1];
    int m_branchCount;
    Rect m_coverSplit;
    ELEMTYPEREAL m_coverSplitArea;
  }; 
 



  Node* AllocNode();

  Branch* AllocBranch();
  void FreeNode(Node* a_node);
  void FreeBranch(Branch* a_branch);
  void InitNode(Node* a_node);
  void InitRect(Rect* a_rect);
  ACTION InsertRectRec(Rect* a_rect, const DATATYPE& a_id, Node* a_node, Node** a_newNode, int a_level, ListBranch** reInsertBranchList, bool* reinsarr);
  bool InsertRect(Rect* a_rect, const DATATYPE& a_id, Node** a_root, int a_level, bool* reinsarr);
  Rect NodeCover(Node* a_node);
  int NodeClean(Node* a_node);
  ACTION AddBranch(Branch* a_branch, Node* a_node, Node** a_newNode, ListBranch** reInsertBranchList, bool* reinsarr);
  void DisconnectBranch(Node* a_node, int a_index);
  int PickBranch(Rect* a_rect, Node* a_node);
  Rect CombineRect(Rect* a_rectA, Rect* a_rectB);
  Rect ComputeRectCenter(Rect* a_rectA);
  ELEMTYPEREAL ComputeRectDist(Rect* a_rectA, Rect* a_rectB);
  void SplitNode(Node* a_node, Branch* a_branch, Node** a_newNode);
  ELEMTYPEREAL RectSphericalVolume(Rect* a_rect);
  ELEMTYPEREAL RectVolume(Rect* a_rect);
  ELEMTYPEREAL CalcRectVolume(Rect* a_rect);
  void GetBranches(Node* a_node, Branch* a_branch, PartitionVars* a_parVars);
  void ChoosePartition(PartitionVars* a_parVars, int a_minFill);
  void ChoosePartitionTemporally(PartitionVars* a_parVars, int a_minFill); //AR
  void ChoosePartitionTrajectory(PartitionVars* a_parVars, int a_minFill);//AR
  void LoadNodes(Node* a_nodeA, Node* a_nodeB, PartitionVars* a_parVars);
  void InitParVars(PartitionVars* a_parVars, int a_maxRects, int a_minFill);
  void PickSeeds(PartitionVars* a_parVars);
  void Classify(int a_index, int a_group, PartitionVars* a_parVars);
  bool RemoveRect(Rect* a_rect, const DATATYPE& a_id, Node** a_root, bool* reinsarr);
  bool RemoveRectRec(Rect* a_rect, const DATATYPE& a_id, Node* a_node, ListNode** a_listNode, bool* reinsarr);
 
  bool RemoveEmptyLeafUpdateNodeCover(Rect* a_rect, int a_id, Node** a_root, bool* reinsarr,bool del);
  bool RemoveEmptyLeafUpdateNodeCoverRec(Rect* a_rect, int a_id, Node* a_node, ListNode** a_listNode, bool* reinsarr,bool del);

  
  ListNode* AllocListNode();
  void FreeListNode(ListNode* a_listNode);
  ListBranch* AllocListBranch();
  void FreeListBranch(ListBranch* a_listBranch);
  bool Overlap(Rect* a_rectA, Rect* a_rectB);
  ELEMTYPEREAL GetOverlap(Rect* a_rectA, Rect* a_rectB);
  ELEMTYPEREAL GetMargin(Rect* a_rectA);
  void ReInsertNode(Node* a_node, ListNode** a_listNode);
  void ReInsertBranch(Branch* a_branch, int a_level, ListBranch** a_listBranch);
  bool Search(Node* a_node, Rect* a_rect, int& a_foundCount, bool __cdecl a_resultCallback(DATATYPE a_data, void* a_context), void* a_context);
  bool SearchRecent(Node* a_node, Rect* a_rect, int& a_foundCount, bool __cdecl a_resultCallback(DATATYPE a_data, void* a_context), void* a_context);
  void RemoveAllRec(Node* a_node);
  void Reset();
  void CountRec(Node* a_node, int& a_count, int* currentcount);
  void ShowStatsInternal(Node* a_node, int& numRec, int*& nodeAtL, int*& brachesAtL);
  
  Node* m_root;                                    ///< Root of tree
  ELEMTYPEREAL m_unitSphereVolume;                 ///< Unit sphere constant for required number of dimensions
};


RTREE_TEMPLATE
RTREE_QUAL::RTree()
{
  ASSERT(MAXNODES > MINNODES);
  ASSERT(MINNODES > 0);


  // We only support machine word size simple data type eg. integer index or object pointer.
  // Since we are storing as union with non data branch
  ASSERT(sizeof(DATATYPE) == sizeof(void*) || sizeof(DATATYPE) == sizeof(int));

  // Precomputed volumes of the unit spheres for the first few dimensions
  const double UNIT_SPHERE_VOLUMES[] = {
    0.000000f, 2.000000f, 3.141593f, // Dimension  0,1,2
    4.188790f, 4.934802f, 5.263789f, // Dimension  3,4,5
    5.167713f, 4.724766f, 4.058712f, // Dimension  6,7,8
    3.298509f, 2.550164f, 1.884104f, // Dimension  9,10,11
    1.335263f, 0.910629f, 0.599265f, // Dimension  12,13,14
    0.381443f, 0.235331f, 0.140981f, // Dimension  15,16,17
    0.082146f, 0.046622f, 0.025807f, // Dimension  18,19,20 
  };

  MDM = new MDM2D();
  nl = new NodeList();
  globalCnt = 0;
  numdExpired =0;
  globalNID = 0;
  nodeRead = 0;
  nodeWrite = 0;
  leafNodeRead = 0;
  visitedNotFound =0;
  leafNodeReadSearch = 0;
  leafNodeWrite = 0;
  leafNodeWriteDueToCleaning =0;
  LARGE_INTEGER RecurrentTimeStamp;
  QueryPerformanceCounter(&RecurrentTimeStamp);
  cleanTime =0;
  updateTime=0;

  m_root = AllocNode();
  m_root->m_level = 0;
  //link to the node list
  nl->InsertHash(m_root->m_nid, (void*)m_root);
  
  m_unitSphereVolume = (ELEMTYPEREAL)UNIT_SPHERE_VOLUMES[NUMDIMS];
  rootLevel = m_root->m_level;

  queryOutfile.open (QUERY_OUT_FILE);
  
}


RTREE_TEMPLATE
RTREE_QUAL::~RTree()
{
  Reset(); // Free, or reset node memory
  delete MDM;
  delete nl;
  MDM = NULL;
  queryOutfile.close();
}


RTREE_TEMPLATE
void RTREE_QUAL::Insert(const ELEMTYPE a_min[NUMDIMS], const ELEMTYPE a_max[NUMDIMS], const DATATYPE& a_dataId)
{
#ifdef _DEBUG
  for(int index=0; index<NUMDIMS; ++index)
  {
    ASSERT(a_min[index] <= a_max[index]);
  }
#endif //_DEBUG

  Rect rect;
  
  for(int axis=0; axis<NUMDIMS; ++axis)
  {
    rect.m_min[axis] = a_min[axis];
    rect.m_max[axis] = a_max[axis];
  }

  a_dataId->order = globalCnt++;

 
   
  a_dataId->duration = DEFAULT_DURATION;
  
  InsertRect(&rect, a_dataId, &m_root, 0, NULL);
}


RTREE_TEMPLATE
void RTREE_QUAL::Register(const ELEMTYPE a_min[NUMDIMS], const ELEMTYPE a_max[NUMDIMS], const DATATYPE& a_dataId)
{
	
#ifdef _DEBUG
  for(int index=0; index<NUMDIMS; ++index)
  {
    ASSERT(a_min[index] <= a_max[index]);
  }
#endif //_DEBUG

  Rect rect;
  
  for(int axis=0; axis<NUMDIMS; ++axis)
  {
    rect.m_min[axis] = a_min[axis];
    rect.m_max[axis] = a_max[axis];
  }
   int oid = a_dataId->oid;
   int order = a_dataId->order;
   double start = a_min[2];

   a_dataId->duration = DEFAULT_DURATION;

   InsertRect(&rect, a_dataId, &m_root, 0, NULL);

  //special for LDR-tree
 
  MDMEntry2D* ment = MDM->SearchOid(oid);
  //if exist, update the location information without incrementing MDN
  if(ment)
  {
	  MDM->UpdateHash(ment,a_dataId->order, start);
  }
 
}


RTREE_TEMPLATE
void RTREE_QUAL::Remove(const ELEMTYPE a_min[NUMDIMS], const ELEMTYPE a_max[NUMDIMS], const DATATYPE& a_dataId)
{
#ifdef _DEBUG
  for(int index=0; index<NUMDIMS; ++index)
  {
    ASSERT(a_min[index] <= a_max[index]);
  }
#endif //_DEBUG

  Rect rect;
  
  for(int axis=0; axis<NUMDIMS; ++axis)
  {
    rect.m_min[axis] = a_min[axis];
    rect.m_max[axis] = a_max[axis];
  }

  RemoveRect(&rect, a_dataId, &m_root, NULL);
}
//AR postpone drop untill later

RTREE_TEMPLATE
void RTREE_QUAL::Drop(const ELEMTYPE a_min[NUMDIMS], const ELEMTYPE a_max[NUMDIMS], const DATATYPE& a_dataId)
{
#ifdef _DEBUG
  for(int index=0; index<NUMDIMS; ++index)
  {
    ASSERT(a_min[index] <= a_max[index]);
  }
#endif //_DEBUG

  //special for LDR-tree
  int oid = a_dataId->oid;
  int order = globalCnt++;

  MDMEntry2D* ment = MDM->SearchOid(oid);
  //if exist, update the entry to be (oid, order, MDN+1)
  if(ment)
  {
	  MDM->UpdateHash(ment, order);
	  MDM->ChangeMDN(ment, 1);
  }
  //else, insert an MDM entry (oid, order, 1);
  else
  {
	  MDM->InsertHash(oid, order);
  }
}


RTREE_TEMPLATE
void RTREE_QUAL::Update(const ELEMTYPE a_min[NUMDIMS], const ELEMTYPE a_max[NUMDIMS], const DATATYPE& a_dataId)
{
#ifdef _DEBUG
  for(int index=0; index<NUMDIMS; ++index)
  {
    ASSERT(a_min[index] <= a_max[index]);
  }
#endif //_DEBUG

  Rect rect;
  
  for(int axis=0; axis<NUMDIMS; ++axis)
  {
    rect.m_min[axis] = a_min[axis];
    rect.m_max[axis] = a_max[axis];
  }
  a_dataId->duration = DEFAULT_DURATION;
  //special for LDR-tree
  int oid = a_dataId->oid;
  int order =  a_dataId->order;
  double start  =a_min[2] ;

  InsertRect(&rect, a_dataId, &m_root, 0, NULL);

  MDMEntry2D* ment = MDM->SearchOid(oid);
  //if exist, update the location information and increment MDN
  if(ment)
  {

	  MDM->UpdateHash(ment, order,start);
	  MDM->ChangeMDN(ment, 1);
  }
  //else, insert an MDM entry (oid, newloc, 1);
  else
  {
	  MDM->InsertHash(oid,order, start,1);
  }
  clock_t startTime, finish;
  startTime = clock();
  //clean manager CT or LRC
  if (CLEANOPTION == CLEANINGTOKENS)
  {
	  if(order%NUMUPDATESPERTOKEN == 0)
	  {
		  for(int i=0; i<NUMTOKENS; i++)
		  {
				NodeListEntry* nle = nl->GetNextTokenPos();
				if(nle){
					int remaing;
				Rect rect = NodeCover((Node*)(nle->nptr));
				int nodeID = ((Node*)(nle->nptr))->m_nid;
				remaing =NodeClean((Node*)(nle->nptr));
				
					if(remaing==0){
						 RemoveEmptyLeafUpdateNodeCover(&rect,nodeID, &m_root, NULL,true);
					}
					else 
						RemoveEmptyLeafUpdateNodeCover(&rect,nodeID, &m_root, NULL,false);
				
				}
		  }
	  }
  }
  else  if (CLEANOPTION == LRCLIST)
  {
	if((NUMTOKENS != 0) && (order%(NUMUPDATESPERTOKEN/NUMTOKENS) == 0))
	  {
			NodeListEntry* nle = nl->leafHead;
			if(nle)
			{
				int remaing;
				Rect rect = NodeCover((Node*)(nle->nptr));
				int nodeID = ((Node*)(nle->nptr))->m_nid;
				remaing =NodeClean((Node*)(nle->nptr));


				if(remaing==0){
					 RemoveEmptyLeafUpdateNodeCover(&rect,nodeID, &m_root, NULL,true);
				}
				else{
					nl->MoveToLeafTail(nle);
					RemoveEmptyLeafUpdateNodeCover(&rect,nodeID, &m_root, NULL,false);
				}
			}
	  }
  }
	finish = clock();
	cleanTime+=(finish-startTime);
}







RTREE_TEMPLATE
int RTREE_QUAL::Search(const ELEMTYPE a_min[NUMDIMS], const ELEMTYPE a_max[NUMDIMS], bool __cdecl a_resultCallback(DATATYPE a_data, void* a_context), void* a_context)
{
#ifdef _DEBUG
  for(int index=0; index<NUMDIMS; ++index)
  {
    ASSERT(a_min[index] <= a_max[index]);
  }
#endif //_DEBUG

  Rect rect;
  
  for(int axis=0; axis<NUMDIMS; ++axis)
  {
    rect.m_min[axis] = a_min[axis];
    rect.m_max[axis] = a_max[axis];
  }

  // NOTE: May want to return search result another way, perhaps returning the number of found elements here.

  int foundCount = 0;
  Search(m_root, &rect, foundCount, a_resultCallback, a_context);

  return foundCount;
}




RTREE_TEMPLATE
void RTREE_QUAL::CountReset()
{
  nodeRead = 0;
  nodeWrite = 0;
  leafNodeRead = 0;
  leafNodeWrite = 0;
  numdExpired = 0;
  cleanTime =0;
  updateTime=0;
  visitedNotFound=0;
}


RTREE_TEMPLATE
int RTREE_QUAL::Count(int* currentCount)
{
  int totalCount = 0;
  *currentCount = 0;

  CountRec(m_root, totalCount, currentCount);
  
  return totalCount;
}


RTREE_TEMPLATE
void RTREE_QUAL::CountRec(Node* a_node, int& a_count, int* currentCount)
{
  if(a_node->IsInternalNode())  // not a leaf node
  {
    for(int index = 0; index < a_node->m_count; ++index)
    {
      CountRec(a_node->m_branch[index].m_child, a_count, currentCount);
    }
  }
  else // A leaf node
  {
    a_count += a_node->m_count;

	for(int index = 0; index < a_node->m_count; ++index)
	{
		int oid = a_node->m_branch[index].m_data->oid;			
		MDMEntry2D* ment = MDM->SearchOid(oid);
		if(!ment)
			(*currentCount)++;
		else if(ment->order == a_node->m_branch[index].m_data->order)
			(*currentCount)++;
	}
  }
}

//show statistics of the R-tree
RTREE_TEMPLATE
void RTREE_QUAL::ShowStats()
{
	int treeHeight = m_root->m_level + 1; //height of the three
    int numRecords = 0; //the number of records on leaf level
	int* nodeAtLevel = new int[treeHeight]; //for keeping the number of nodes at each level
	int* branchesAtLevel = new int[treeHeight]; //for keeping the number of branches at each level

	for(int i = 0; i<treeHeight; i++) //initialize the two arrays
	{
		nodeAtLevel[i] = 0;
		branchesAtLevel[i] = 0;
	}

    ShowStatsInternal(m_root, numRecords, nodeAtLevel, branchesAtLevel);
 
	int numNodes = 0; //the total number of nodes
	int numBranches = 0; //the total number of branches
	int numInternalNodes = 0; //the number of internal nodes
	int numInternalNodesBase = 0; //changes by yasin
	int numInternalNodesNonBase = 0; //changes by yasin
	int numLeafNodes = nodeAtLevel[0]; //the number of leaf nodes

	double avgUtil = 0; //the average node utilization
	
	for(int j = 0; j<treeHeight; j++)
	{
		numNodes+=nodeAtLevel[j];
		numBranches+=branchesAtLevel[j];
	}

	numInternalNodes = numNodes - numLeafNodes;
	numInternalNodesBase = nodeAtLevel[1];//changed by yasin
    numInternalNodesNonBase = numInternalNodes - numInternalNodesBase;//changed by yasin 

	avgUtil = ((double)numBranches)/(numNodes*MAXNODES);

	printf("\n\n\n*************LIMITED Retention*****************\n");
	printf("Settings: NUMDIM = %d, BRANCHSIZE = %d, NODE_BRACH_SIZE = %d, MAXNodes = %d, MINNodes = %d\n", NUMDIMS, sizeof(Branch), sizeof(Branch)*MAXNODES, MAXNODES, MINNODES);
	printf("Settings: NUM_OBJECTS = %d, NUM_QUERIES = %d, MAX_NUMBER_UPDATES = %d, DEFAULT_DURATION = %d\n", NUM_OBJECTS, NUM_QUERIES, MAX_NUMBER_UPDATES, DEFAULT_DURATION);


	//calculate size of the tree in bytes	
	int nodeIntermedioNoBaseSIZE = (sizeof(Node) + 4)*numInternalNodesNonBase;//1 pointer (up)=4
	int nodeIntermedioBaseSIZE;
	
	//BEFORE: if (LRCLIST == 1)nodeIntermedioBaseSIZE = (sizeof(Node) + 12 + MAXNODES*4)*numInternalNodesBase;//if LRC with pointers to list are used.1 extra pointer per branch entry
    if (LRCLIST == 1)nodeIntermedioBaseSIZE = (sizeof(Node) + 4)*numInternalNodesBase;//1 pointer (up)
    else nodeIntermedioBaseSIZE =(sizeof(Node) +12)*numInternalNodesBase;//3 pointers (up, next, previous)=12
	int nodeLeafSIZE = (sizeof(Node) + MAXNODES*4)*numLeafNodes;//timestamp=(MAXNODES*4), oid=(MAXNODES*0) because oid is index to data (pointer)

	printf("Height of tree = %d\n", treeHeight);
	printf("Number of nodes = %d\n", numNodes);
	printf("Number of internal nodes = %d\n", numInternalNodes);
	printf("Number of NON-BASE internal nodes = %d\n", numInternalNodesNonBase);
	printf("Number of BASE internal nodes = %d\n", numInternalNodesBase);
	printf("Number of leaf nodes = %d\n", numLeafNodes);
	printf("Number of leaf entries = %d\n", numRecords);

	printf("-----------------------------------------------\n");
    printf("Size of 1 NON-BASE internal nodes = %d\n", (sizeof(Node) +4));//node + pointer to parent
	if (LRCLIST == 1) printf("Size of 1 BASE internal nodes = %d\n", (sizeof(Node) + 4));//node + pointer to parent
	else printf("Size of 1 BASE internal nodes = %d\n", (sizeof(Node) + 12));//node + pointer to parent and siblings
	printf("Size of 1 LEAF nodes = %d\n", (sizeof(Node) + MAXNODES*4));//timestamp=(MAXNODES*4)

	printf("Total Size of all internal nodes = %d\n", nodeIntermedioBaseSIZE+nodeIntermedioNoBaseSIZE);
	printf("Total Size of all NON-BASE internal nodes = %d\n", nodeIntermedioNoBaseSIZE);
	printf("Total Size of all BASE internal nodes = %d\n", nodeIntermedioBaseSIZE);
	printf("Total Size of all LEAF nodes = %d\n", nodeLeafSIZE);
	printf("==>TOTAL Size of tree (bytes) = %d\n", nodeIntermedioBaseSIZE+nodeIntermedioNoBaseSIZE+nodeLeafSIZE);
	printf("AVERAGE Size of node (bytes) = %f\n", (double)(nodeIntermedioBaseSIZE+nodeIntermedioNoBaseSIZE+nodeLeafSIZE)/(double)numNodes);
	
	printf("-----------------------------------------------\n");

	printf("Overall average node utilization = %.3f\n", avgUtil);
	printf("Nodes per level (Leaf level at the end):\n    ");
	int index ;
	for(int index = treeHeight-1; index>=0; index--)
	{
		printf("%d   ", nodeAtLevel[index]);
	}
	printf("\n");
	
	printf("Node utilization per level (Leaf level at the end):\n    ");
	for(index = treeHeight-1; index>=0; index--)
	{
		printf("%.2f   ", ((double)branchesAtLevel[index])/(nodeAtLevel[index]*MAXNODES));
	}
    printf("\nOverall node visit(r+w): %d,  Leaf node vist(r+w): %d", nodeRead+nodeWrite, leafNodeRead+leafNodeWrite);
	//I/O
    printf("\n\n-->TOTAL I/O: %d",leafNodeRead+leafNodeWrite);
	printf("\n\n-->TOTAL I/O for search : %d",leafNodeReadSearch);
	printf("\n\n-->TOTAL visited nodes with no found results : %d",visitedNotFound);

	//size
	printf("\nNumber of MDM (UM) entries: %d", MDM->size);
	printf("\nNumber of obsolete disk entries: %d", MDM->TotalCount);
	int UpdateMemo = MDM->size*(4+4+2);
	printf("\nSize of UM (bytes): %d ", UpdateMemo);

	int LRCListSize;
	int LRCCompactListSize;
	int TokensSize; 
	if (LRCLIST == 1){//LRC (with cleaning tokens)
		LRCListSize = numLeafNodes*sizeof(NodeListEntry);//we could also use timestamp and node ID (4+4) instead of sizeof(NodeListEntry)
		LRCCompactListSize = numLeafNodes*(4+4);//we could also use timestamp and node ID (4+4) instead of sizeof(NodeListEntry)
		TokensSize = 0;// all the information is obtained from LRC list  
		printf("\nSize of LRC List (bytes): %d ", LRCListSize);
		printf("\nSize of tokens (bytes): %d ", TokensSize);
		printf("\n==>Size of additional structures (UM+LRCList+Tokens) (bytes): %d ", UpdateMemo+LRCListSize+TokensSize);
		printf("\n==>TOTAL SIZE (RUM-Tree + additional structures) (bytes): %d ",
			UpdateMemo+LRCListSize+TokensSize + nodeIntermedioBaseSIZE+nodeIntermedioNoBaseSIZE+nodeLeafSIZE);
		printf("\n\nSize of LRC Compact List (bytes): %d ", LRCCompactListSize);
		printf("\nSize of additional structures (UM+CompactLRCList+Tokens) (bytes): %d ", UpdateMemo+LRCCompactListSize+TokensSize);
		printf("\nTOTAL SIZE (RUM-Tree + additional compact structures) (bytes): %d ",
			UpdateMemo+LRCCompactListSize+TokensSize + nodeIntermedioBaseSIZE+nodeIntermedioNoBaseSIZE+nodeLeafSIZE);

	}
	else if (CLEANINGTOKENS == 1){//Cleaning Tokens (alone)
		LRCListSize = 0;//we could also use timestamp and node ID (4+4) instead of sizeof(NodeListEntry)
		TokensSize = NUMTOKENS * (4+4+4);//3 pointers (current, begin, end)
		printf("\nSize of tokens (bytes): %d ", TokensSize);
		printf("\n==>Size of additional structures (UM+Tokens) (bytes): %d ", UpdateMemo+TokensSize);
		printf("\n==>TOTAL SIZE (RUM-Tree + additional structures) (bytes): %d ",
			UpdateMemo+TokensSize + nodeIntermedioBaseSIZE+nodeIntermedioNoBaseSIZE+nodeLeafSIZE);

	}
	printf("\n number of expired and deleted : %d ", numdExpired);
	printf("\n time for clean: %f time of update  : %f ", cleanTime/CLOCKS_PER_SEC,updateTime/CLOCKS_PER_SEC);
	printf("\n number of expired and deleted : %d ", numdExpired);
	printf("\n****************************************\n");
	//printf("\n\nNumber of IO writes due to leaf nodes cleaning: %d ", leafNodeWriteDueToCleaning);

	
	delete nodeAtLevel;
	delete branchesAtLevel;

	return;
}


//called by ShowStats() to recursively process
RTREE_TEMPLATE
void RTREE_QUAL::ShowStatsInternal(Node* a_node, int& numRec, int*& nodeAtL, int*& branchesAtL)
{
  //ASSERT(NUMDIMS == 3);

  if(a_node->IsInternalNode())  // not a leaf node
  {
	nodeAtL[a_node->m_level]++;
	branchesAtL[a_node->m_level]+= a_node->m_count;

    for(int index = 0; index < a_node->m_count; ++index)
    {
      ShowStatsInternal(a_node->m_branch[index].m_child, numRec, nodeAtL, branchesAtL);
    }
  }
  else // A leaf node
  {
    numRec+= a_node->m_count;
	nodeAtL[a_node->m_level]++;
	branchesAtL[a_node->m_level]+= a_node->m_count;
  }
}

RTREE_TEMPLATE
void RTREE_QUAL::RemoveAll()
{
  // Delete all existing nodes
  Reset();

  MDM->deleteAllEntries();
  nl->deleteAllEntries();

  m_root = AllocNode();
  m_root->m_level = 0;
  //link to the node list
  nl->InsertHash(m_root->m_nid, (void*)m_root);

  rootLevel = m_root->m_level;
  nodeRead = 0;
  nodeWrite = 0;
  leafNodeRead = 0;
  leafNodeReadSearch = 0;
  leafNodeWrite = 0;
  numdExpired =0;
  cleanTime = 0;
  updateTime =0;
}


RTREE_TEMPLATE
void RTREE_QUAL::Reset()
{
#ifdef RTREE_DONT_USE_MEMPOOLS
  // Delete all existing nodes
  RemoveAllRec(m_root);
#else // RTREE_DONT_USE_MEMPOOLS
  // Just reset memory pools.  We are not using complex types
  // EXAMPLE
#endif // RTREE_DONT_USE_MEMPOOLS
}


RTREE_TEMPLATE
void RTREE_QUAL::RemoveAllRec(Node* a_node)
{
  ASSERT(a_node);
  ASSERT(a_node->m_level >= 0);

  if(a_node->IsInternalNode()) // This is an internal node in the tree
  {
    for(int index=0; index < a_node->m_count; ++index)
    {
      RemoveAllRec(a_node->m_branch[index].m_child);
    }
  }
  else
  {
    for(int index=0; index < a_node->m_count; ++index)
    {
      delete a_node->m_branch[index].m_data;
    }
  }

  FreeNode(a_node); 
}


RTREE_TEMPLATE
typename RTREE_QUAL::Node* RTREE_QUAL::AllocNode()
{
  Node* newNode;
#ifdef RTREE_DONT_USE_MEMPOOLS
  newNode = new Node;
#else // RTREE_DONT_USE_MEMPOOLS
  // EXAMPLE
#endif // RTREE_DONT_USE_MEMPOOLS
  InitNode(newNode);

   //assign the node ID
  newNode->m_nid = globalNID++;

  nodeWrite++;
  leafNodeWrite++;

  return newNode;
}

RTREE_TEMPLATE
typename RTREE_QUAL::Branch* RTREE_QUAL::AllocBranch()
{
  Branch* newBranch;
#ifdef RTREE_DONT_USE_MEMPOOLS
  newBranch = new Branch;
#else // RTREE_DONT_USE_MEMPOOLS
  // EXAMPLE
#endif // RTREE_DONT_USE_MEMPOOLS
  return newBranch;
}

RTREE_TEMPLATE
void RTREE_QUAL::FreeNode(Node* a_node)
{
  ASSERT(a_node);

  //de-link the node from the node list
  if(a_node->IsLeaf())
  {
		NodeListEntry* dn = nl->SearchNID(a_node->m_nid);
		ASSERT(dn);
		nl->DeleteHash(dn);
  }

  nodeWrite++;
  leafNodeWrite++;

#ifdef RTREE_DONT_USE_MEMPOOLS
  delete a_node;
#else // RTREE_DONT_USE_MEMPOOLS
  // EXAMPLE
#endif // RTREE_DONT_USE_MEMPOOLS
}

RTREE_TEMPLATE
void RTREE_QUAL::FreeBranch(Branch* a_branch)
{
  ASSERT(a_branch);

#ifdef RTREE_DONT_USE_MEMPOOLS
  delete a_branch;
#else // RTREE_DONT_USE_MEMPOOLS
  // EXAMPLE
#endif // RTREE_DONT_USE_MEMPOOLS
}

// Allocate space for a node in the list used in DeletRect to
// store Nodes that are too empty.
RTREE_TEMPLATE
typename RTREE_QUAL::ListNode* RTREE_QUAL::AllocListNode()
{
#ifdef RTREE_DONT_USE_MEMPOOLS
  return new ListNode;
#else // RTREE_DONT_USE_MEMPOOLS
  // EXAMPLE
#endif // RTREE_DONT_USE_MEMPOOLS
}


RTREE_TEMPLATE
void RTREE_QUAL::FreeListNode(ListNode* a_listNode)
{
#ifdef RTREE_DONT_USE_MEMPOOLS
  delete a_listNode;
#else // RTREE_DONT_USE_MEMPOOLS
  // EXAMPLE
#endif // RTREE_DONT_USE_MEMPOOLS
}

// Allocate space for a branch in the list 
RTREE_TEMPLATE
typename RTREE_QUAL::ListBranch* RTREE_QUAL::AllocListBranch()
{
#ifdef RTREE_DONT_USE_MEMPOOLS
  return new ListBranch;
#else // RTREE_DONT_USE_MEMPOOLS
  // EXAMPLE
#endif // RTREE_DONT_USE_MEMPOOLS
}

RTREE_TEMPLATE
void RTREE_QUAL::FreeListBranch(ListBranch* a_listBranch)
{
#ifdef RTREE_DONT_USE_MEMPOOLS
  delete a_listBranch;
#else // RTREE_DONT_USE_MEMPOOLS
  // EXAMPLE
#endif // RTREE_DONT_USE_MEMPOOLS
}

RTREE_TEMPLATE
void RTREE_QUAL::InitNode(Node* a_node)
{
  a_node->m_count = 0;
  a_node->m_level = -1;
  }


RTREE_TEMPLATE
void RTREE_QUAL::InitRect(Rect* a_rect)
{
  for(int index = 0; index < NUMDIMS; ++index)
  {
    a_rect->m_min[index] = (ELEMTYPE)0;
    a_rect->m_max[index] = (ELEMTYPE)0;
  }
}


// Inserts a new data rectangle into the index structure.
// Recursively descends tree, propagates splits back up.
// Returns SPLIT if node was split.  
// Returns REINSERT if node entries were reinserted
// Returns NONE if no action was performed
// If node was split, sets the pointer pointed to by
// new_node to point to the new node.  Old node updated to become one of two.
// The level argument specifies the number of steps up from the leaf
// level to insert; e.g. a data rectangle goes in at level = 0.
RTREE_TEMPLATE
ACTION RTREE_QUAL::InsertRectRec(Rect* a_rect, const DATATYPE& a_id, Node* a_node, Node** a_newNode, int a_level, ListBranch** reInsertBranchList, bool* reinsarr)
{
  ASSERT(a_rect && a_node && a_newNode);
  ASSERT(a_level >= 0 && a_level <= a_node->m_level);

  int index;
  Branch branch;
  Node* otherNode;

  nodeRead++;
  nodeWrite++;

  // Still above level for insertion, go down tree recursively
  if(a_node->m_level > a_level)
  {
    if(a_node->m_count==0){
		cout<<"error "<<endl;
		return NONE;
	}
    index = PickBranch(a_rect, a_node);
	ACTION ret = InsertRectRec(a_rect, a_id, a_node->m_branch[index].m_child, &otherNode, a_level, reInsertBranchList, reinsarr);
    if (ret == NONE)
    {
      // Child was not split or reinsert
      //a_node->m_branch[index].m_rect = CombineRect(a_rect, &(a_node->m_branch[index].m_rect));
      if(a_node->m_branch[index].m_child->m_count >= MINNODES)
      {
        // just resize parent rect
        a_node->m_branch[index].m_rect = NodeCover(a_node->m_branch[index].m_child);
      }
      else if (a_node->m_count>1)
      {
		  // child(ren) removed, not enough entries in node, eliminate the branch
		  //DisconnectBranch(a_node, index);
		  for(int i= a_node->m_branch[index].m_child->m_count-1; i>=0; i--)
		  {
				ReInsertBranch(&a_node->m_branch[index].m_child->m_branch[i], a_node->m_branch[index].m_child->m_level, reInsertBranchList);
				DisconnectBranch(a_node->m_branch[index].m_child, i);
		  }
		  FreeNode(a_node->m_branch[index].m_child);
		  DisconnectBranch(a_node, index);
      }

      return NONE;
    }
    else if(ret == SPLIT)
    {
      a_node->m_branch[index].m_rect = NodeCover(a_node->m_branch[index].m_child);
      branch.m_child = otherNode;
      branch.m_rect = NodeCover(otherNode);
      return AddBranch(&branch, a_node, a_newNode, reInsertBranchList, reinsarr);
    }
	else //ret == REINSERT
	{
		a_node->m_branch[index].m_rect = NodeCover(a_node->m_branch[index].m_child);
		return REINSERT;
	}
  }
  else if(a_node->m_level == a_level) // Have reached level for insertion. Add rect, split if necessary
  {
	bool clean = false;
	if(CLEANWHILEINSERT == ALWAYS)
		clean = true;

	if(a_node->m_level == 0)
	{
		leafNodeRead++;
		leafNodeWrite++;
	}

	// delete old entries from a_node
	//AR this is node clean 
	if(clean && (a_node->m_level == 0))
	{
	//	bool* disconnectBran = new bool[MAXNODES]; //alloc
		bool disconnectBran[MAXNODES];
		int k;
		for( k = 0; k<MAXNODES; k++)
		   disconnectBran[k] = false;
		int index;
		
		for( index = 0; index < a_node->m_count; ++index)
		{
			//AR first checking for expired entries that have exceeded its duration
			if(a_node->m_branch[index].m_data->vmin.v[2]+a_node->m_branch[index].m_data->duration<=currentTimeStamp)
			{
					MDMEntry2D* ment = MDM->SearchOid(a_node->m_branch[index].m_data->oid);
					if(ment){
						MDM->removeExpiredTimeStampIfExisits(ment,a_node->m_branch[index].m_data->order);
					}
					disconnectBran[index] = true; //to remove this node;
					numdExpired++; 
			}
			if(a_node->m_branch[index].m_data->vmax.v[2] == NOW_TIME){
				MDMEntry2D* ment = MDM->SearchOid(a_node->m_branch[index].m_data->oid);
				if(ment){
					double e= MDM->getEndTimeRemoveUpdate(ment,a_node->m_branch[index].m_data->order);
						if(e!=-1){	 
								a_node->m_branch[index].m_data->vmax.v[2]=e;
								a_node->m_branch[index].m_rect.m_max[2]=e;
								MDM->ChangeMDN(ment, -1);
						}
				}
			}
			
			
		}
		for (k=MAXNODES-1; k>=0; k--)
		{
		  if(disconnectBran[k])
		  {
			delete a_node->m_branch[k].m_data;
			DisconnectBranch(a_node, k);
		  }
		}

		//delete disconnectBran;  //dealloc

		//move the node to the end of the LRC list
		if(CLEANOPTION == LRCLIST)
		{
			NodeListEntry* nle = nl->SearchNID(a_node->m_nid);
			ASSERT(nle);
			nl->MoveToLeafTail(nle);
		}
	}//if
    
	branch.m_rect = *a_rect;
    branch.m_child = (Node*) a_id;

    // Child field of leaves contains id of data record
    return AddBranch(&branch, a_node, a_newNode, reInsertBranchList, reinsarr);

	
  }
  else
  {
    // Should never occur
    ASSERT(0);
    return NONE;
  }
}


// Insert a data rectangle into an index structure.
// InsertRect provides for splitting the root;
// returns 1 if root was split, 0 if it was not.
// The level argument specifies the number of steps up from the leaf
// level to insert; e.g. a data rectangle goes in at level = 0.
// InsertRect2 does the recursion.
RTREE_TEMPLATE
bool RTREE_QUAL::InsertRect(Rect* a_rect, const DATATYPE& a_id, Node** a_root, int a_level, bool* reinsarr)
{
  ASSERT(a_rect && a_root);
  ASSERT(a_level >= 0 && a_level <= (*a_root)->m_level);
#ifdef _DEBUG
  for(int index=0; index < NUMDIMS; ++index)
  {
    ASSERT(a_rect->m_min[index] <= a_rect->m_max[index]);
  }
#endif //_DEBUG  

  Node* newRoot;
  Node* newNode;
  Branch branch;
  bool deletearr = false;
  bool ret = false;

  if(!reinsarr)
  {
	  deletearr = true;
	  reinsarr = new bool[(*a_root)->m_level+2];
	  for(int i=0; i<(*a_root)->m_level+2; i++)
		  reinsarr[i]=false;
  }

  ListBranch* reInsertBranchList = NULL;

  ACTION action = InsertRectRec(a_rect, a_id, *a_root, &newNode, a_level, &reInsertBranchList, reinsarr);
  if(action == SPLIT)  // Root split
  {
    newRoot = AllocNode();  // Grow tree taller and new root
    newRoot->m_level = (*a_root)->m_level + 1;
 	//link to the node list
	if(newRoot->IsLeaf())
		nl->InsertHash(newRoot->m_nid, (void*)newRoot);
 
    branch.m_rect = NodeCover(*a_root);
    branch.m_child = *a_root;
    AddBranch(&branch, newRoot, NULL, &reInsertBranchList, reinsarr);
    branch.m_rect = NodeCover(newNode);
    branch.m_child = newNode;
    AddBranch(&branch, newRoot, NULL, &reInsertBranchList, reinsarr);
    *a_root = newRoot;
	rootLevel = (*a_root)->m_level;
    ret = true;
  }

  Branch* tempBranch;
   // Reinsert any branches from removed nodes
//  int insiderBefore = Count(); //&

   while(reInsertBranchList)
   {
     tempBranch = reInsertBranchList->m_branch;

     InsertRect(&(tempBranch->m_rect),
                tempBranch->m_data,
                a_root,
                reInsertBranchList->m_level,
			    reinsarr);
  
//	 int insiderAfter = Count(); //&

     ListBranch* remLBranch = reInsertBranchList;
     reInsertBranchList = reInsertBranchList->m_next;
      
     FreeBranch(remLBranch->m_branch);
     FreeListBranch(remLBranch);
   }

  if(deletearr)
	 delete reinsarr;
  return ret;
}


// Find the smallest rectangle that includes all rectangles in branches of a node.
RTREE_TEMPLATE
typename RTREE_QUAL::Rect RTREE_QUAL::NodeCover(Node* a_node)
{
  ASSERT(a_node);
  
  int firstTime = true;
  Rect rect;
  InitRect(&rect);
  
  for(int index = 0; index < a_node->m_count; ++index)
  {
    if(firstTime)
    {
      rect = a_node->m_branch[index].m_rect;
      firstTime = false;
    }
    else
    {
      rect = CombineRect(&rect, &(a_node->m_branch[index].m_rect));
    }
  }
  
  return rect;
}

// For every entry in a leaf node, check to see whether the entry is obsolete due to an existing
// MDM entry, if yes, delete the entry
//AR in the cleaning process we first remove expired entries 
// we also check the recent flag of the object in the tree by comparing the value in the memo with the value in the token 
RTREE_TEMPLATE
int RTREE_QUAL::NodeClean(Node* a_node)
{
		ASSERT(a_node);
		ASSERT(a_node->m_level == 0);
		bool writeback = false;
		
		//bool* disconnectBran = new bool[MAXNODES]; //alloc
		bool disconnectBran[MAXNODES];
		int k;
		for( k = 0; k<MAXNODES; k++)
		   disconnectBran[k] = false;
		int index;
				
		
		for( index = 0; index < a_node->m_count; ++index)
		{
			//AR first checking for expired entries that have exceeded its duration
			if(a_node->m_branch[index].m_data->vmin.v[2]+a_node->m_branch[index].m_data->duration<=currentTimeStamp)
			{
				    MDMEntry2D* ment = MDM->SearchOid(a_node->m_branch[index].m_data->oid);
					if(ment){
						MDM->removeExpiredTimeStampIfExisits(ment,a_node->m_branch[index].m_data->order);
					}
					disconnectBran[index] = true; //to remove this node;
					numdExpired++; 
			}
		
			if(a_node->m_branch[index].m_data->vmax.v[2]/*a_node->m_branch[index].m_data->end */== NOW_TIME){
				MDMEntry2D* ment = MDM->SearchOid(a_node->m_branch[index].m_data->oid);
				if(ment){
					double e= MDM->getEndTimeRemoveUpdate(ment,a_node->m_branch[index].m_data->order);
						if(e!=-1){	 
							
								a_node->m_branch[index].m_data->vmax.v[2]=e;
						    	a_node->m_branch[index].m_rect.m_max[2]=e;
								MDM->ChangeMDN(ment, -1);
						}
				}
			}
			
			
		}
	
		for (k=MAXNODES-1; k>=0; k--)
		{
		  if(disconnectBran[k])
		  {
			delete a_node->m_branch[k].m_data;
			DisconnectBranch(a_node, k);
			writeback = true;
		  }
		}

		//delete disconnectBran; //dealloc

		leafNodeRead++;
		if(writeback){
			leafNodeWrite++;
			leafNodeWriteDueToCleaning++;
		}
		return a_node->m_count; //returing the remaining entries in the node
}


// Add a branch to a node. 
// Reinsert some branches if the node is full for the first time;
// Split the node if the node is full again (not the first time)
// Returns REINSERT if node entries are re-inserted
// Returns SPLIT if node entries are split
// Returns NONE if no future actions
RTREE_TEMPLATE
ACTION RTREE_QUAL::AddBranch(Branch* a_branch, Node* a_node, Node** a_newNode, ListBranch** reInsertBranchList, bool* reinsarr)
{
  ASSERT(a_branch);
  ASSERT(a_node);

  if(a_node->m_count < MAXNODES)  // Split won't be necessary
  {
    a_node->m_branch[a_node->m_count] = *a_branch;
    ++a_node->m_count;

    return NONE;
  }

  //now should have (a_node->m_count == MAXNODES) guaranteed
  if((a_node->m_level != m_root->m_level) && (!reinsarr[a_node->m_level])) //need reinsert
  {
//	  int temp = Count();
	  bool reinserted = false; //the a_branch is reinserted or not
	  //compute the big rectangle
	  Rect R = NodeCover(a_node);
	  R = CombineRect(&R, &a_branch->m_rect); //combine with the inserted one
	  Rect CenterR = ComputeRectCenter(&R);

	  //compute the distance from center of each rectangle to the big rectangle
	//  EleEntry* distarr = new EleEntry[MAXNODES+1]; //alloc
	  EleEntry distarr[MAXNODES+1];
	  int i;
	  for( i = 0; i<MAXNODES; i++)
	  {
		  distarr[i].indexnum = i;
		  distarr[i].field = ComputeRectDist(&a_node->m_branch[i].m_rect, &CenterR);
	  }
	  distarr[i].indexnum = i;
	  distarr[i].field = ComputeRectDist(&a_branch->m_rect, &CenterR);

	  //sort the distance in decreasing order
	  qsort((void *)distarr, MAXNODES+1, sizeof(EleEntry), compareEleEntryDec);

	  //remove the first p=REINSERTPERCENT entries from a_node
	  if((int)(MAXNODES*REINSERTPERCENT) >=1)
	  {
		//  bool* disconnectBran = new bool[MAXNODES]; //alloc
		  bool disconnectBran[MAXNODES];
		  int k;
		  for( k = 0; k<MAXNODES; k++)
			  disconnectBran[k] = false;

		  //if the percentage contains more than zero entry to reinsert
		  int i;
		  for(i=0; i< (int)(MAXNODES*REINSERTPERCENT); i++)
		  {
			  int order = distarr[i].indexnum;
			  if(order != MAXNODES)
			  {
				ReInsertBranch(&a_node->m_branch[order], a_node->m_level, reInsertBranchList);
				disconnectBran[order] = true;
			  }
			  else
			  {
				reinserted = true;
				ReInsertBranch(a_branch, a_node->m_level, reInsertBranchList);
			  }
		  }

		  for (k=MAXNODES-1; k>=0; k--)
		  {
			  if(disconnectBran[k])
			  {
				DisconnectBranch(a_node, k);
			  }
		  }

		  //delete disconnectBran; //dealloc
	  }
	  else
	  {
		  //A special case: no reinsert technique
		  reinserted = true;
		  ReInsertBranch(a_branch, a_node->m_level, reInsertBranchList);
	  }

	  //note: the MBR of a_node will be adjusted after return

	 // delete distarr; //dealloc
	  reinsarr[a_node->m_level] = true;

	  //if the branch is not added, add it to the node
	  if(!reinserted)
	  {
		a_node->m_branch[a_node->m_count] = *a_branch;
		++a_node->m_count;
	  }

	  return REINSERT;
  }
  else
  {
    ASSERT(a_newNode);
    
	reinsarr[a_node->m_level] = true;
    SplitNode(a_node, a_branch, a_newNode);
    return SPLIT;
  }
}


// Disconnect a dependent node.
// Caller must return (or stop using iteration index) after this as count has changed
RTREE_TEMPLATE
void RTREE_QUAL::DisconnectBranch(Node* a_node, int a_index)
{
  ASSERT(a_node && (a_index >= 0) && (a_index < MAXNODES));
  ASSERT(a_node->m_count > 0);

  // Remove element by swapping with the last element to prevent gaps in array
  a_node->m_branch[a_index] = a_node->m_branch[a_node->m_count - 1];
  
  --a_node->m_count;
}


// Pick a branch. If the node points to leaves, choose the one needs lease overlap 
// enlargement, resolve tie by choosing the entry whose rectangle needs lease area enlargement
// (First sort the rectangles in the node in increasing order of their area enlargement needed to include 
// the new data rectangle. Choose the first p entries, considering all entries in the node, 
// chhoose the entry needs lease overlpa enlargement). If the node does not point to leaves, 
// choose the entry whose rectangle needs least area enlargement to include the new data. 

RTREE_TEMPLATE
int RTREE_QUAL::PickBranch(Rect* a_rect, Node* a_node)
{
  if(R_TREE ==0){
 //AR comment this code temporarly to test the diffrence in performace between r tree and r * tree 
  ASSERT(a_rect && a_node);
  
  bool firstTime = true;
  ELEMTYPEREAL increase;
  ELEMTYPEREAL overlap;
  ELEMTYPEREAL bestIncr = (ELEMTYPEREAL)-1;
  ELEMTYPEREAL area, newarea, validspace, leastoverlap;
  int best;
  Rect* curRect;
  Rect tempRect, e;
  bool completeFit = false;
  int maxexam; //the number of entries in order need to check


  //EleEntry* enlarr = new EleEntry[MAXNODES]; //alloc
  EleEntry enlarr[MAXNODES];
  //For each entry
  int index;
  for( index=0; index < a_node->m_count; ++index)
  {
	//calculate the area enlargement
    curRect = &a_node->m_branch[index].m_rect;
    area = CalcRectVolume(curRect);
    tempRect = CombineRect(a_rect, curRect);
	newarea = CalcRectVolume(&tempRect);
    increase = newarea - area;

	//if completely fit in a previous entry rectangle
    if (completeFit) 
	{
      if (increase == 0.0) 
	  { // it completely fits 
        if (area < validspace) 
		{
          validspace= area;
          best= index;
        }
      }
    }
    else //otherwise if not completely fit in previous entry rectangles
	{
      if (increase == 0.0) 
	  {
        validspace= area;
        best= index;
        completeFit= true;
      }
      else 
	  {
        enlarr[index].field= increase;
		enlarr[index].indexnum= index;
      }
    }
  }//for

  //if no entry can complelely enclose the data without enlarging
  if (!completeFit) 
  {
	qsort((void *)enlarr, a_node->m_count, sizeof(EleEntry), compareEleEntryInc);
	  
	//if the node points to leaves
    if (a_node->m_level == 1) 
	{     
      maxexam = (a_node->m_count < MAXENTRYTOCHECK)? a_node->m_count:MAXENTRYTOCHECK;
	  int i;
      for ( i= 0; i < maxexam; i++) 
	  {
		// Create enlargedrect 
		curRect = &a_node->m_branch[enlarr[i].indexnum].m_rect;
		tempRect = CombineRect(a_rect, curRect);
        
        overlap= 0.0;
		int k;
        for ( k= 0; k < a_node->m_count; k++)
		{
          if (k != i) 
		  {
			  e= a_node->m_branch[enlarr[k].indexnum].m_rect;
			  overlap= overlap + GetOverlap(&tempRect, &e) - GetOverlap(curRect, &e);
          }
        }

        if (firstTime) 
		{
          leastoverlap= overlap;
          best = enlarr[i].indexnum;
          firstTime= false;
        }
        else 
		{
          if (overlap < leastoverlap) 
		  {
            leastoverlap= overlap;
            best = enlarr[i].indexnum;
          }
        }
      }
    }
    else 
	{ // Subtrees are not leafs 
      best = enlarr[0].indexnum;
    }
  }

  //delete [] enlarr;  //dealloc
  if(best<0 || best>= a_node->m_count)
	  best = 0;

  return best;
 }else{

 ASSERT(a_rect && a_node);
  
  bool firstTime = true;
  ELEMTYPEREAL increase;
  ELEMTYPEREAL bestIncr = (ELEMTYPEREAL)-1;
  ELEMTYPEREAL area;
  ELEMTYPEREAL bestArea;
  int best;
  Rect tempRect;

  for(int index=0; index < a_node->m_count; ++index)
  {
    Rect* curRect = &a_node->m_branch[index].m_rect;
    area = CalcRectVolume(curRect);
    tempRect = CombineRect(a_rect, curRect);
    increase = CalcRectVolume(&tempRect) - area;
    if((increase < bestIncr) || firstTime)
    {
      best = index;
      bestArea = area;
      bestIncr = increase;
      firstTime = false;
    }
    else if((increase == bestIncr) && (area < bestArea))
    {
      best = index;
      bestArea = area;
      bestIncr = increase;
    }
  }
  return best;
  }
}

// Combine two rectangles into larger one containing both
RTREE_TEMPLATE
typename RTREE_QUAL::Rect RTREE_QUAL::CombineRect(Rect* a_rectA, Rect* a_rectB)
{
  ASSERT(a_rectA && a_rectB);

  Rect newRect;

  for(int index = 0; index < NUMDIMS; ++index)
  {
    newRect.m_min[index] = Min(a_rectA->m_min[index], a_rectB->m_min[index]);
    newRect.m_max[index] = Max(a_rectA->m_max[index], a_rectB->m_max[index]);
  }

  return newRect;
}

// Compute the centroid of a given rectangle
RTREE_TEMPLATE
typename RTREE_QUAL::Rect RTREE_QUAL::ComputeRectCenter(Rect* a_rectA)
{
  ASSERT(a_rectA);

  Rect CenterRect;

  for(int index = 0; index < NUMDIMS; ++index)
  {
    CenterRect.m_min[index] = CenterRect.m_max[index] = (a_rectA->m_min[index] + a_rectA->m_max[index])/2;
  }

  return CenterRect;
}

// Compute the distance of two rectangles (between their centroids)
RTREE_TEMPLATE
ELEMTYPEREAL RTREE_QUAL::ComputeRectDist(Rect* a_rectA, Rect* a_rectB)
{
  ASSERT(a_rectA && a_rectB);

  Rect CRA, CRB;
  CRA = ComputeRectCenter(a_rectA);
  CRB = ComputeRectCenter(a_rectB);
  ELEMTYPEREAL dist = 0;

  for(int index = 0; index < NUMDIMS; ++index)
  {
	  dist+= (double)fabs(a_rectA->m_min[index] - a_rectB->m_min[index]) * (double)fabs(a_rectA->m_min[index] - a_rectB->m_min[index]);
  }

  dist  = (double)sqrt(dist);
  return dist;
  
}


// Split a node.
// Divides the nodes branches and the extra one between two nodes.
// Old node is one of the new ones, and one really new one is created.
// Tries more than one method for choosing a partition, uses best result.
RTREE_TEMPLATE
void RTREE_QUAL::SplitNode(Node* a_node, Branch* a_branch, Node** a_newNode)
{
  ASSERT(a_node);
  ASSERT(a_branch);

  // Could just use local here, but member or external is faster since it is reused
  PartitionVars localVars;
  PartitionVars* parVars = &localVars;
  int level;

  // Load all the branches into a buffer, initialize old node
  level = a_node->m_level;
  GetBranches(a_node, a_branch, parVars);

  // Find partition
  if(level ==0){
	  if(NODE_SPLIT == 1)
		  ChoosePartitionTemporally(parVars, MINNODES);
	  else if (NODE_SPLIT == 2)
		  ChoosePartitionTrajectory(parVars, MINNODES);
	  else 
		  ChoosePartition(parVars, MINNODES);
  }
  else 
	ChoosePartition(parVars, MINNODES);

  // Put branches from buffer into 2 nodes according to chosen partition
  *a_newNode = AllocNode();
  (*a_newNode)->m_level = level; 
  //link to the node list
  if((*a_newNode)->IsLeaf())
		nl->InsertHash((*a_newNode)->m_nid, (void*)(*a_newNode));

  int orilevel = a_node->m_level;
  a_node->m_level = level;

  if((orilevel > 0) && (level == 0))
  {
	    //link to the node list
		nl->InsertHash(a_node->m_nid, (void*)a_node);
  }
  else if((orilevel == 0) && (level > 0))
  {
	  //de-link the node from the node list
	  NodeListEntry* dn = nl->SearchNID(a_node->m_nid);
	  ASSERT(dn);
	  nl->DeleteHash(dn); 
  }

  LoadNodes(a_node, *a_newNode, parVars);
  
  ASSERT((a_node->m_count + (*a_newNode)->m_count) == parVars->m_total);
}


// Calculate the n-dimensional volume of a rectangle
RTREE_TEMPLATE
ELEMTYPEREAL RTREE_QUAL::RectVolume(Rect* a_rect)
{
  ASSERT(a_rect);
  
  ELEMTYPEREAL volume = (ELEMTYPEREAL)1;

  for(int index=0; index<NUMDIMS; ++index)
  {
    volume *= a_rect->m_max[index] - a_rect->m_min[index];
  }
  
  ASSERT(volume >= (ELEMTYPEREAL)0);
  
  return volume;
}


// The exact volume of the bounding sphere for the given Rect
RTREE_TEMPLATE
ELEMTYPEREAL RTREE_QUAL::RectSphericalVolume(Rect* a_rect)
{
  ASSERT(a_rect);
   
  ELEMTYPEREAL sumOfSquares = (ELEMTYPEREAL)0;
  ELEMTYPEREAL radius;

  for(int index=0; index < NUMDIMS; ++index) 
  {
    ELEMTYPEREAL halfExtent = ((ELEMTYPEREAL)a_rect->m_max[index] - (ELEMTYPEREAL)a_rect->m_min[index]) * 0.5f;
    sumOfSquares += halfExtent * halfExtent;
  }

  radius = (ELEMTYPEREAL)sqrt(sumOfSquares);
  
  // Pow maybe slow, so test for common dims like 2,3 and just use x*x, x*x*x.
  if(NUMDIMS == 3)
  {
    return (radius * radius * radius * m_unitSphereVolume);
  }
  else if(NUMDIMS == 2)
  {
    return (radius * radius * m_unitSphereVolume);
  }
  else
  {
    return (ELEMTYPEREAL)(pow(radius, NUMDIMS) * m_unitSphereVolume);
  }
}


// Use one of the methods to calculate retangle volume
RTREE_TEMPLATE
ELEMTYPEREAL RTREE_QUAL::CalcRectVolume(Rect* a_rect)
{
#ifdef RTREE_USE_SPHERICAL_VOLUME
  return RectSphericalVolume(a_rect); // Slower but helps certain merge cases
#else // RTREE_USE_SPHERICAL_VOLUME
  return RectVolume(a_rect); // Faster but can cause poor merges
#endif // RTREE_USE_SPHERICAL_VOLUME  
}


// Load branch buffer with branches from full node plus the extra branch.
RTREE_TEMPLATE
void RTREE_QUAL::GetBranches(Node* a_node, Branch* a_branch, PartitionVars* a_parVars)
{
  ASSERT(a_node);
  ASSERT(a_branch);

  ASSERT(a_node->m_count == MAXNODES);
    
  // Load the branch buffer
  int index;
  for( index=0; index < MAXNODES; ++index)
  {
    a_parVars->m_branchBuf[index] = a_node->m_branch[index];
  }
  a_parVars->m_branchBuf[MAXNODES] = *a_branch;
  a_parVars->m_branchCount = MAXNODES + 1;

  // Calculate rect containing all in the set
  a_parVars->m_coverSplit = a_parVars->m_branchBuf[0].m_rect;
  for(index=1; index < MAXNODES+1; ++index)
  {
    a_parVars->m_coverSplit = CombineRect(&a_parVars->m_coverSplit, &a_parVars->m_branchBuf[index].m_rect);
  }
  a_parVars->m_coverSplitArea = CalcRectVolume(&a_parVars->m_coverSplit);

  InitNode(a_node);
}


// Method for choosing a partition:
// Refer to Beckmann's 1990 paper for the principal of the algorithm
RTREE_TEMPLATE
void RTREE_QUAL::ChoosePartition(PartitionVars* a_parVars, int a_minFill)
{
	
	ASSERT(a_parVars);
//	ELEMTYPEREAL biggestDiff;
//	int group, chosen, betterGroup;
  
	InitParVars(a_parVars, a_parVars->m_branchCount, a_minFill);

    //1. choose split axis
	int split_axis;
    double minmarg = FLT_MAX;
	//EleEntry* lowarr = new EleEntry[a_parVars->m_branchCount];  //alloc
	//EleEntry* higharr = new EleEntry[a_parVars->m_branchCount]; //alloc
	EleEntry lowarr [BRANCHPERNODE+1] ;
	EleEntry higharr [BRANCHPERNODE+1];
	Rect R1, R2;

    for (int dim = 0; dim < NUMDIMS; dim++)
    // for every dimension
    {
        for (int index = 0; index < a_parVars->m_branchCount; index++)
        {
            lowarr[index].indexnum = index;
            lowarr[index].field = a_parVars->m_branchBuf[index].m_rect.m_min[dim];
			higharr[index].indexnum = index;
            higharr[index].field = a_parVars->m_branchBuf[index].m_rect.m_max[dim];            
        }

        // Sort by lower and upper value perpendicular axis
		qsort((void *)lowarr, a_parVars->m_branchCount, sizeof(EleEntry), compareEleEntryInc);
		qsort((void *)higharr, a_parVars->m_branchCount, sizeof(EleEntry), compareEleEntryInc);
	
		int l;
		int k;
        double marg = 0.0;
        // for all possible distributions of lowarr
        for ( k = 0; k < a_parVars->m_branchCount-2*MINNODES+1; k++)
        {
			R1 = a_parVars->m_branchBuf[lowarr[0].indexnum].m_rect;
			R2 = a_parVars->m_branchBuf[lowarr[a_parVars->m_branchCount-1].indexnum].m_rect;
            // now calculate margin of R1
			int l;
            for ( l = 0; l < MINNODES+k; l++)
            {
				R1 = CombineRect(&R1, &a_parVars->m_branchBuf[lowarr[l].indexnum].m_rect);
            }
		    marg += GetMargin(&R1);
            // now calculate margin of R2
            for ( ; l < a_parVars->m_branchCount; l++)
            {
				
				R2 = CombineRect(&R2, &a_parVars->m_branchBuf[lowarr[l].indexnum].m_rect);
            }
		    marg += GetMargin(&R2); 
        }
	    // for all possible distributions of higharr
        for (k = 0; k < a_parVars->m_branchCount-2*MINNODES+1; k++)
        {
			R1 = a_parVars->m_branchBuf[higharr[0].indexnum].m_rect;
			R2 = a_parVars->m_branchBuf[higharr[a_parVars->m_branchCount-1].indexnum].m_rect;
            // now calculate margin of R1
            for ( l = 0; l < MINNODES+k; l++)
            {
				R1 = CombineRect(&R1, &a_parVars->m_branchBuf[higharr[l].indexnum].m_rect);
            }
		    marg += GetMargin(&R1);

            // now calculate margin of R2
            for ( ; l < a_parVars->m_branchCount; l++)
            {
				R2 = CombineRect(&R2, &a_parVars->m_branchBuf[higharr[l].indexnum].m_rect);
            }
		    marg += GetMargin(&R2); 
        }

        // actual margin better than optimum?
        if (marg < minmarg)
        {
            split_axis = dim;
            minmarg = marg;
        }
    } //for every dimension

    
    //-----------------------------------------------------------------
    // choose best distribution for split axis
    for (int index = 0; index < a_parVars->m_branchCount; index++)
    {
        lowarr[index].indexnum = index;
        lowarr[index].field = a_parVars->m_branchBuf[index].m_rect.m_min[split_axis];
		higharr[index].indexnum = index;
        higharr[index].field = a_parVars->m_branchBuf[index].m_rect.m_max[split_axis];            
    }

    // Sort by lower and upper value perpendicular axis
	qsort((void *)lowarr, a_parVars->m_branchCount, sizeof(EleEntry), compareEleEntryInc);
	qsort((void *)higharr, a_parVars->m_branchCount, sizeof(EleEntry), compareEleEntryInc);

    double minover = FLT_MAX;
    double mindead = FLT_MAX;
	int dist;
	bool low;
    
	// for all possible distributions of lowarr and higharr
    for (int k = 0; k < a_parVars->m_branchCount-2*MINNODES+1; k++)
    {
		R1 = a_parVars->m_branchBuf[lowarr[0].indexnum].m_rect;
		R2 = a_parVars->m_branchBuf[lowarr[a_parVars->m_branchCount-1].indexnum].m_rect;

        // lower sort
		// now calculate dead space of R1
        double dead = 0.0;
		double over = 0.0;
		int l;
		for ( l = 0; l < MINNODES+k; l++)
		{
			R1 = CombineRect(&R1, &a_parVars->m_branchBuf[lowarr[l].indexnum].m_rect);
			dead-= CalcRectVolume(&a_parVars->m_branchBuf[lowarr[l].indexnum].m_rect);
		}
        dead += CalcRectVolume(&R1);
	
		// now calculate margin of R2
		for ( ; l < a_parVars->m_branchCount; l++)
		{
			R2 = CombineRect(&R2, &a_parVars->m_branchBuf[lowarr[l].indexnum].m_rect);
			dead-= CalcRectVolume(&a_parVars->m_branchBuf[lowarr[l].indexnum].m_rect);
		}
        dead += CalcRectVolume(&R2);
		over = GetOverlap(&R1, &R2); 

        if ((over < minover) || ((over == minover) && (dead < mindead)))
        {
            minover = over;
            mindead = dead;
            dist = MINNODES+k;
            low = true;
        }

        // upper sort
		// now calculate margin of R1
		R1 = a_parVars->m_branchBuf[higharr[0].indexnum].m_rect;
		R2 = a_parVars->m_branchBuf[higharr[a_parVars->m_branchCount-1].indexnum].m_rect;
        dead = 0.0;
		for (l = 0; l < MINNODES+k; l++)
		{
			R1 = CombineRect(&R1, &a_parVars->m_branchBuf[higharr[l].indexnum].m_rect);
			dead-= CalcRectVolume(&a_parVars->m_branchBuf[higharr[l].indexnum].m_rect);
		}
        dead += CalcRectVolume(&R1);
	
		// now calculate margin of R2
		for ( ; l < a_parVars->m_branchCount; l++)
		{
			R2 = CombineRect(&R2, &a_parVars->m_branchBuf[higharr[l].indexnum].m_rect);
			dead-= CalcRectVolume(&a_parVars->m_branchBuf[higharr[l].indexnum].m_rect);
		}
        dead += CalcRectVolume(&R2);
		over = GetOverlap(&R1, &R2); 

	    if ((over < minover) || ((over == minover) && (dead < mindead)))
        {
            minover = over;
            mindead = dead;
            dist = MINNODES+k;
            low = false;
        }
    }// for all possible distributions of lowarr and higharr

	//-----------------------------------------------------
    // distribute entries to two nodes
	EleEntry* curarr;
	if(low)
		curarr = lowarr;
	else
		curarr = higharr;

	for (int l = 0; l < a_parVars->m_branchCount; l++)
	{
		if(l<dist)
			Classify(curarr[l].indexnum, 0, a_parVars);
		else
			Classify(curarr[l].indexnum, 1, a_parVars);
	}

	//delete [] lowarr;  //dealloc
	//delete [] higharr; //dealloc

	ASSERT((a_parVars->m_count[0] + a_parVars->m_count[1]) == a_parVars->m_total);
	ASSERT((a_parVars->m_count[0] >= a_parVars->m_minFill) && (a_parVars->m_count[1] >= a_parVars->m_minFill));
}
//AR this splitting is based on start time of each entry half of the entries in one group and the other half in the other group
RTREE_TEMPLATE
void RTREE_QUAL::ChoosePartitionTemporally(PartitionVars* a_parVars, int a_minFill){
		
 ASSERT(a_parVars);
  
  ELEMTYPEREAL biggestDiff;
  int group, chosen, betterGroup;
  
  InitParVars(a_parVars, a_parVars->m_branchCount, a_minFill);
  
  EleEntry* sortArray = new EleEntry[a_parVars->m_branchCount];
  for (int index = 0; index < a_parVars->m_branchCount; index++)
  {
		sortArray[index].indexnum = index;
		sortArray[index].field =a_parVars->m_branchBuf[index].m_data->vmin.v[2];
		        
  }

  // Sort by start time of the entry 
  qsort((void *)sortArray, a_parVars->m_branchCount, sizeof(EleEntry), compareEleEntryInc);

  int slpitIndex = a_parVars->m_branchCount/2;
  for(int i =0;i<slpitIndex;i++){
	  Classify(sortArray[i].indexnum, 0, a_parVars);
  }
  for(int i =slpitIndex;i<a_parVars->m_branchCount;i++){
	  Classify(sortArray[i].indexnum, 1, a_parVars);
  }
  delete []sortArray;	
  ASSERT((a_parVars->m_count[0] + a_parVars->m_count[1]) == a_parVars->m_total);
  ASSERT((a_parVars->m_count[0] >= a_parVars->m_minFill) && (a_parVars->m_count[1] >= a_parVars->m_minFill));
}
//AR this splitting is based trajecotory id to group trajecotry segments that belong to each other together
RTREE_TEMPLATE
void RTREE_QUAL::ChoosePartitionTrajectory(PartitionVars* a_parVars, int a_minFill){
		
 ASSERT(a_parVars);
  
  ELEMTYPEREAL biggestDiff;
  int group, chosen, betterGroup;
  
  InitParVars(a_parVars, a_parVars->m_branchCount, a_minFill);
  
  EleEntryTraj* sortArray = new EleEntryTraj[a_parVars->m_branchCount];
  for (int index = 0; index < a_parVars->m_branchCount; index++)
  {
		sortArray[index].indexnum = index;
		sortArray[index].id = a_parVars->m_branchBuf[index].m_data->oid;	
		sortArray[index].ts =a_parVars->m_branchBuf[index].m_data->vmin.v[2];
  }

  // Sort by oid of the trajectory 
  qsort((void *)sortArray, a_parVars->m_branchCount, sizeof(EleEntryTraj), compareEleEntryTraj);
  //put the first trajectory to the first partition
  int currOID=sortArray[0].id,i;
  int copyremaing =false;
   i =0;
  int currentParition =0;
  
	for( ; currOID ==(int)sortArray[i].id&&i<a_parVars->m_branchCount; i++)
	{
		Classify(sortArray[i].indexnum, currentParition, a_parVars);
		if(a_parVars->m_count[currentParition] >= a_parVars->m_total - a_parVars->m_minFill){
			copyremaing =true;//partition  is too full copy the remaing into the other partition
			i++;
			break;

		}
	}
	currentParition = 1-currentParition; //if 0 go to 1 and if one go to 0
	if(copyremaing){
		for(; i<a_parVars->m_branchCount; i++)
		{
			Classify(sortArray[i].indexnum, currentParition, a_parVars);
		}
	}else{
		currOID =(int)sortArray[i].id;
		for( ; currOID ==(int)sortArray[i].id&&i<a_parVars->m_branchCount; i++)
		{
			Classify(sortArray[i].indexnum, currentParition, a_parVars);
			if(a_parVars->m_count[currentParition] >= a_parVars->m_total - a_parVars->m_minFill){
				copyremaing =true;//partition  is too full copy the remaing into the other partition
				i++;
				break;

			}
		}
		currentParition = 1-currentParition; //if 0 go to 1 and if one go to 0
		if(copyremaing){
			for(; i<a_parVars->m_branchCount; i++)
			{
				Classify(sortArray[i].indexnum, currentParition, a_parVars);
			}
		}
	}
	
	while (i<a_parVars->m_branchCount){
		currOID =(int)sortArray[i].id;
		Rect* curRect = &a_parVars->m_branchBuf[i].m_rect;
        Rect rect0 = CombineRect(curRect, &a_parVars->m_cover[0]);
        Rect rect1 = CombineRect(curRect, &a_parVars->m_cover[1]);
        ELEMTYPEREAL growth0 = CalcRectVolume(&rect0) - a_parVars->m_area[0];
        ELEMTYPEREAL growth1 = CalcRectVolume(&rect1) - a_parVars->m_area[1];
        ELEMTYPEREAL diff = growth1 - growth0;
        if(diff >= 0)
          currentParition = 0;
        else
          currentParition = 1;
		for( ; currOID ==(int)sortArray[i].id&&i<a_parVars->m_branchCount; i++)
		{
			Classify(sortArray[i].indexnum, currentParition, a_parVars);
			if(a_parVars->m_count[currentParition] >= a_parVars->m_total - a_parVars->m_minFill){
				copyremaing =true;//partition  is too full copy the remaing into the other partition
				i++;
				break;

			}
		}
		currentParition = 1-currentParition; //if 0 go to 1 and if one go to 0
		if(copyremaing){
			for(; i<a_parVars->m_branchCount; i++)
			{
				Classify(sortArray[i].indexnum, currentParition, a_parVars);
			}
		}

	}

  

  
  delete []sortArray;	
  ASSERT((a_parVars->m_count[0] + a_parVars->m_count[1]) == a_parVars->m_total);
  ASSERT((a_parVars->m_count[0] >= a_parVars->m_minFill) && (a_parVars->m_count[1] >= a_parVars->m_minFill));
}
// Copy branches from the buffer into two nodes according to the partition.
RTREE_TEMPLATE
void RTREE_QUAL::LoadNodes(Node* a_nodeA, Node* a_nodeB, PartitionVars* a_parVars)
{
  ASSERT(a_nodeA);
  ASSERT(a_nodeB);
  ASSERT(a_parVars);

  double nAtl=NOW_TIME;
  double nBtl=NOW_TIME;
  double nAtsc=NOW_TIME;
  double nBtsc=NOW_TIME;
  double nAth=0;
  double nBth=0;
  for(int index=0; index < a_parVars->m_total; ++index)
  {
    ASSERT(a_parVars->m_partition[index] == 0 || a_parVars->m_partition[index] == 1);
    
    if(a_parVars->m_partition[index] == 0)
    {
      AddBranch(&a_parVars->m_branchBuf[index], a_nodeA, NULL, NULL, NULL);
    }
    else if(a_parVars->m_partition[index] == 1)
    {
      AddBranch(&a_parVars->m_branchBuf[index], a_nodeB, NULL, NULL, NULL);
    }
  }
  
}


// Initialize a PartitionVars structure.
RTREE_TEMPLATE
void RTREE_QUAL::InitParVars(PartitionVars* a_parVars, int a_maxRects, int a_minFill)
{
  ASSERT(a_parVars);

  a_parVars->m_count[0] = a_parVars->m_count[1] = 0;
  a_parVars->m_area[0] = a_parVars->m_area[1] = (ELEMTYPEREAL)0;
  a_parVars->m_total = a_maxRects;
  a_parVars->m_minFill = a_minFill;
  for(int index=0; index < a_maxRects; ++index)
  {
    a_parVars->m_taken[index] = false;
    a_parVars->m_partition[index] = -1;
  }
}


RTREE_TEMPLATE
void RTREE_QUAL::PickSeeds(PartitionVars* a_parVars)
{
  int seed0, seed1;
  ELEMTYPEREAL worst, waste;
  ELEMTYPEREAL area[MAXNODES+1];

  for(int index=0; index<a_parVars->m_total; ++index)
  {
    area[index] = CalcRectVolume(&a_parVars->m_branchBuf[index].m_rect);
  }

  worst = -a_parVars->m_coverSplitArea - 1;
  for(int indexA=0; indexA < a_parVars->m_total-1; ++indexA)
  {
    for(int indexB = indexA+1; indexB < a_parVars->m_total; ++indexB)
    {
      Rect oneRect = CombineRect(&a_parVars->m_branchBuf[indexA].m_rect, &a_parVars->m_branchBuf[indexB].m_rect);
      waste = CalcRectVolume(&oneRect) - area[indexA] - area[indexB];
      if(waste > worst)
      {
        worst = waste;
        seed0 = indexA;
        seed1 = indexB;
      }
    }
  }
  Classify(seed0, 0, a_parVars);
  Classify(seed1, 1, a_parVars);
}


// Put a branch in one of the groups.
RTREE_TEMPLATE
void RTREE_QUAL::Classify(int a_index, int a_group, PartitionVars* a_parVars)
{
  ASSERT(a_parVars);
  ASSERT(!a_parVars->m_taken[a_index]);

  a_parVars->m_partition[a_index] = a_group;
  a_parVars->m_taken[a_index] = true;

  if (a_parVars->m_count[a_group] == 0)
  {
    a_parVars->m_cover[a_group] = a_parVars->m_branchBuf[a_index].m_rect;
  }
  else
  {
    a_parVars->m_cover[a_group] = CombineRect(&a_parVars->m_branchBuf[a_index].m_rect, &a_parVars->m_cover[a_group]);
  }
  a_parVars->m_area[a_group] = CalcRectVolume(&a_parVars->m_cover[a_group]);
  ++a_parVars->m_count[a_group];
}


// Delete a data rectangle from an index structure.
// Pass in a pointer to a Rect, the tid of the record, ptr to ptr to root node.
// Returns 1 if record not found, 0 if success.
// RemoveRect provides for eliminating the root.
RTREE_TEMPLATE
bool RTREE_QUAL::RemoveRect(Rect* a_rect, const DATATYPE& a_id, Node** a_root, bool* reinsarr)
{
  ASSERT(a_rect && a_root);
  ASSERT(*a_root);

  bool deletearr = false;
  if(!reinsarr)
  {
	  deletearr = true;
	  reinsarr = new bool[(*a_root)->m_level+2];
	  for(int i=0; i<(*a_root)->m_level+2; i++)
		  reinsarr[i]=false;
  }

  Node* tempNode;
  ListNode* reInsertList = NULL;

  if(!RemoveRectRec(a_rect, a_id, *a_root, &reInsertList, reinsarr))
  {
    // Found and deleted a data item
    // Reinsert any branches from eliminated nodes
    while(reInsertList)
    {
      tempNode = reInsertList->m_node;

      for(int index = 0; index < tempNode->m_count; ++index)
      {
        InsertRect(&(tempNode->m_branch[index].m_rect),
                   tempNode->m_branch[index].m_data,
                   a_root,
                   tempNode->m_level,
				   reinsarr);
      }
      
      ListNode* remLNode = reInsertList;
      reInsertList = reInsertList->m_next;
      
      FreeNode(remLNode->m_node);
      FreeListNode(remLNode);
    }
    
    // Check for redundant root (not leaf, 1 child) and eliminate
    if((*a_root)->m_count == 1 && (*a_root)->IsInternalNode())
    {
      tempNode = (*a_root)->m_branch[0].m_child;
      
      ASSERT(tempNode);
      FreeNode(*a_root);
      *a_root = tempNode;
	  rootLevel = (*a_root)->m_level;
    }

	if(deletearr)
		delete reinsarr;
    return false;
  }
  else
  {
	if(deletearr)
		delete reinsarr;
    return true;
  }
}


// Delete an  empty leaf node from node cleaning an index structure.
// Pass in a pointer to a Rect, the tid of the record, ptr to ptr to root node.
// Returns 1 if record not found, 0 if success.
// RemoveEmpty provides for eliminating the root.
RTREE_TEMPLATE
bool RTREE_QUAL::RemoveEmptyLeafUpdateNodeCover(Rect* a_rect, int a_id, Node** a_root, bool* reinsarr,bool del)
{
  ASSERT(a_rect && a_root);
  ASSERT(*a_root);

  bool deletearr = false;
  if(!reinsarr)
  {
	  deletearr = true;
	  reinsarr = new bool[(*a_root)->m_level+2];
	  for(int i=0; i<(*a_root)->m_level+2; i++)
		  reinsarr[i]=false;
  }

  Node* tempNode;
  ListNode* reInsertList = NULL;

  if(!RemoveEmptyLeafUpdateNodeCoverRec(a_rect, a_id, *a_root, &reInsertList, reinsarr,del))
  {
    // Found and deleted a data item
    // Reinsert any branches from eliminated nodes
    while(reInsertList)
    {
      tempNode = reInsertList->m_node;

      for(int index = 0; index < tempNode->m_count; ++index)
      {
        InsertRect(&(tempNode->m_branch[index].m_rect),
                   tempNode->m_branch[index].m_data,
                   a_root,
                   tempNode->m_level,
				   reinsarr);
      }
      
      ListNode* remLNode = reInsertList;
      reInsertList = reInsertList->m_next;
      
      FreeNode(remLNode->m_node);
      FreeListNode(remLNode);
    }
    
    // Check for redundant root (not leaf, 1 child) and eliminate
    if((*a_root)->m_count == 1 && (*a_root)->IsInternalNode())
    {
      tempNode = (*a_root)->m_branch[0].m_child;
      
      ASSERT(tempNode);
      FreeNode(*a_root);
      *a_root = tempNode;
	  rootLevel = (*a_root)->m_level;
    }

	if(deletearr)
		delete reinsarr;
    return false;
  }
  else
  {
	if(deletearr)
		delete reinsarr;
    return true;
  }
}


// Delete a rectangle from non-root part of an index structure.
// Called by RemoveEmptyLeaf.  Descends tree recursively,
// merges branches on the way back up.
// Returns 1 if record not found, 0 if success.
RTREE_TEMPLATE
bool RTREE_QUAL::RemoveEmptyLeafUpdateNodeCoverRec(Rect* a_rect, int a_id, Node* a_node, ListNode** a_listNode, bool* reinsarr,bool del)
{
  ASSERT(a_rect && a_node && a_listNode);
  ASSERT(a_node->m_level >= 0);

  nodeRead++;
  nodeWrite++;

  if(a_node->m_level >1)  // not a leaf node
  {
    for(int index = 0; index < a_node->m_count; ++index)
    {
      if(Overlap(a_rect, &(a_node->m_branch[index].m_rect)))
      {
        if(!RemoveEmptyLeafUpdateNodeCoverRec(a_rect, a_id, a_node->m_branch[index].m_child, a_listNode, reinsarr,del))
        {
          if(a_node->m_branch[index].m_child->m_count >= MINNODES)
          {
            // child removed, just resize parent rect
            a_node->m_branch[index].m_rect = NodeCover(a_node->m_branch[index].m_child);
          }
          else
          {
            // child removed, not enough entries in node, eliminate node
            ReInsertNode(a_node->m_branch[index].m_child, a_listNode);
            DisconnectBranch(a_node, index); // Must return after this call as count has changed
          }
          return false;
        }
      }
    }
    return true;
  }
  else if(a_node->m_level ==1)
  {
	
    for(int index = 0; index < a_node->m_count; ++index)
    {
      if(a_node->m_branch[index].m_child->m_nid == a_id)
      {
		if(del){
		  FreeNode(a_node->m_branch[index].m_child);
			DisconnectBranch(a_node, index); // Must return after this call as count has changed
		}
        return false;
      }
    }
    return true;
  }
}
// Delete a rectangle from non-root part of an index structure.
// Called by RemoveRect.  Descends tree recursively,
// merges branches on the way back up.
// Returns 1 if record not found, 0 if success.
RTREE_TEMPLATE
bool RTREE_QUAL::RemoveRectRec(Rect* a_rect, const DATATYPE& a_id, Node* a_node, ListNode** a_listNode, bool* reinsarr)
{
  ASSERT(a_rect && a_node && a_listNode);
  ASSERT(a_node->m_level >= 0);

  nodeRead++;
  nodeWrite++;

  if(a_node->IsInternalNode())  // not a leaf node
  {
    for(int index = 0; index < a_node->m_count; ++index)
    {
      if(Overlap(a_rect, &(a_node->m_branch[index].m_rect)))
      {
        if(!RemoveRectRec(a_rect, a_id, a_node->m_branch[index].m_child, a_listNode, reinsarr))
        {
          if(a_node->m_branch[index].m_child->m_count >= MINNODES)
          {
            // child removed, just resize parent rect
            a_node->m_branch[index].m_rect = NodeCover(a_node->m_branch[index].m_child);
          }
          else
          {
            // child removed, not enough entries in node, eliminate node
            ReInsertNode(a_node->m_branch[index].m_child, a_listNode);
            DisconnectBranch(a_node, index); // Must return after this call as count has changed
          }
          return false;
        }
      }
    }
    return true;
  }
  else // A leaf node
  {
	leafNodeRead++;
	leafNodeWrite++;

    for(int index = 0; index < a_node->m_count; ++index)
    {
      if(a_node->m_branch[index].m_child == (Node*)a_id)
      {
		delete a_node->m_branch[index].m_data;
        DisconnectBranch(a_node, index); // Must return after this call as count has changed
        return false;
      }
    }
    return true;
  }
}


// Decide whether two rectangles overlap.
RTREE_TEMPLATE
bool RTREE_QUAL::Overlap(Rect* a_rectA, Rect* a_rectB)
{
  ASSERT(a_rectA && a_rectB);

  for(int index=0; index < NUMDIMS; ++index)
  {
    if (a_rectA->m_min[index] > a_rectB->m_max[index] ||
        a_rectB->m_min[index] > a_rectA->m_max[index])
    {
      return false;
    }
  }
  return true;
}

// Return the amount of overlap of two rectangles
RTREE_TEMPLATE
ELEMTYPEREAL RTREE_QUAL::GetOverlap(Rect* a_rectA, Rect* a_rectB)
{
  if(!Overlap(a_rectA, a_rectB))
	  return 0;

  double low, high;
  int i;
  double spc= 1.0;

  for (i= 0; i < NUMDIMS; i++) 
  {
	low = (a_rectA->m_min[i] < a_rectB->m_min[i])? a_rectB->m_min[i]: a_rectA->m_min[i];
	high = (a_rectA->m_max[i] < a_rectB->m_max[i])? a_rectA->m_max[i]: a_rectB->m_max[i];
    spc= spc * (high-low);
  }

  return spc;
}


// Return the margin of a rectangle. 
RTREE_TEMPLATE
ELEMTYPEREAL RTREE_QUAL::GetMargin(Rect* a_rectA)
{
  ASSERT(a_rectA);

  double margin= 0.0;

  for (int i= 0; i < NUMDIMS; i++) 
  {
	  margin+= a_rectA->m_max[i] - a_rectA->m_min[i];
  }

  return margin;
}

// Add a node to the reinsertion list.  All its branches will later
// be reinserted into the index structure.
RTREE_TEMPLATE
void RTREE_QUAL::ReInsertNode(Node* a_node, ListNode** a_listNode)
{
  ListNode* newListNode;

  newListNode = AllocListNode();
  newListNode->m_node = a_node;
  newListNode->m_next = *a_listNode;
  *a_listNode = newListNode;
}

// Add a branch to the reinsertion list.  All the branches will later
// be reinserted into the index structure.
RTREE_TEMPLATE
void RTREE_QUAL::ReInsertBranch(Branch* a_branch, int a_level, ListBranch** a_listBranch)
{
  ListBranch* newListBranch;

  newListBranch = AllocListBranch();
  newListBranch->m_branch = AllocBranch();
  newListBranch->m_branch->m_rect = a_branch->m_rect;
  newListBranch->m_branch->m_data = a_branch->m_data;
  newListBranch->m_level = a_level;
  //xpx

  newListBranch->m_next = *a_listBranch;
  *a_listBranch = newListBranch;
}


// Search in an index tree or subtree for all data retangles that overlap the argument rectangle.
//AR this function should return all entries that are with in time window recent or not
//this function should not return expired entries
RTREE_TEMPLATE
bool RTREE_QUAL::Search(Node* a_node, Rect* a_rect, int& a_foundCount, bool __cdecl a_resultCallback(DATATYPE a_data, void* a_context), void* a_context)
{
  ASSERT(a_node);
  ASSERT(a_node->m_level >= 0);
  ASSERT(a_rect);
  LocEntry3D *p;
  nodeRead++;
  struct greater{
	  bool operator()(const LocEntry3D& a,const LocEntry3D& b) const{
		
		  return a.order<b.order;
     }
	};
  if(a_node->IsInternalNode()) // This is an internal node in the tree
  {
    for(int index=0; index < a_node->m_count; ++index)
    {
      if(Overlap(a_rect, &a_node->m_branch[index].m_rect))
      {
			if(!Search(a_node->m_branch[index].m_child, a_rect, a_foundCount, a_resultCallback, a_context))
			{
				return false; // Don't continue searching
			}
		
      }
    }
  }
  else // This is a leaf node
  {
	//this part is for debugging
	//**************************************************************
	/*
	  if( a_node->m_count ==0)
		return true;
	 
	long nodeTmin=9999999999999999999,nodeTmax=-1;
	 double ql =( (queryContex *)a_context )->qtl;
	double qh =( (queryContex *)a_context )->qth;
	for(int index=0; index < a_node->m_count; ++index)
	{
		if(a_node->m_branch[index].m_data->start<nodeTmin)
			nodeTmin = a_node->m_branch[index].m_data->start;
		if(a_node->m_branch[index].m_data->end>nodeTmax)
			nodeTmax = a_node->m_branch[index].m_data->end;
	}
	if(!(!(qh<nodeTmin)&& !(ql>nodeTmax)) )
		return true;
		*/
	//****************************************************************
	leafNodeRead++;
	leafNodeReadSearch ++;
	bool foundinthisnode = false;
    for(int index=0; index < a_node->m_count; ++index)
    {
	  bool newentry = false;

	  if(a_node->m_branch[index].m_data->vmin.v[2]+a_node->m_branch[index].m_data->duration>=currentTimeStamp){
		  //First check overlap, then identify new or old
		   double qtl =( (queryContex *)a_context )->qtl;
		   double qth =( (queryContex *)a_context )->qth;
	//
		  if(Overlap(a_rect, &a_node->m_branch[index].m_rect))
		  {
			  double endTime =a_node->m_branch[index].m_data->vmax.v[2];
			  //now check if there is an end time for this entry 
			  if(a_node->m_branch[index].m_data->vmax.v[2] == NOW_TIME){
				  int oid = a_node->m_branch[index].m_data->oid;
				  MDMEntry2D* ment = MDM->SearchOid(oid);
				  if(ment){
					double e = MDM->getEndTime(ment,a_node->m_branch[index].m_data->order);
					if(e !=-1)
						endTime = e;
				  }
			  }
			  if((a_node->m_branch[index].m_data->vmin.v[2]+a_node->m_branch[index].m_data->duration) >=currentTimeStamp
				  &&!(qth<a_node->m_branch[index].m_data->vmin.v[2])&& !(qtl>endTime) ){//temporal overlap between nodes
				DATATYPE& id = a_node->m_branch[index].m_data;
				if(&a_resultCallback)
				{
					//AR this is for debugging purposes
					//queryOutfile << a_node->m_branch[index].m_data->oid<<" "<<std::fixed<<a_node->m_branch[index].m_data->vmin.v[2]<<" "<<std::fixed<<endTime<<
					//	" "<<std::fixed<<a_node->m_branch[index].m_data->vmin.v[0]<<" "<<std::fixed<<a_node->m_branch[index].m_data->vmin.v[1]<<endl;
					foundinthisnode = true;
					++a_foundCount;
					 if(!a_resultCallback(id, a_context))
					 {
						return false; // Don't continue searching
					 }
				}
			  }
		  }
		}
	}
	
	if(foundinthisnode){

		foundinthisnode =true;
		//cout<<"found Node id = "<<a_node->m_nid<<endl;
		;;
	}
	else {
		visitedNotFound ++;
		//cout<<"visited Node id = "<<a_node->m_nid<<endl;
	}
  }
  return true; // Continue searching
}






#undef RTREE_TEMPLATE
#undef RTREE_QUAL

#endif //RTREE_H
